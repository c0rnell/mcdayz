package ru.xlv.mcdayz.item;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumAction;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;

public abstract class ItemUsable extends ItemBase {

    protected int maxUses;
    private EnumAction action;

    ItemUsable(String name, int maxUses, EnumAction action) {
        super(name);
        this.setHasSubtypes(true);
        this.setMaxDamage(0);
        this.maxUses = maxUses;
        this.action = action;
    }

    protected void setUses(ItemStack itemStack, int uses) {
        if(itemStack.getTagCompound() == null) itemStack.setTagCompound(new NBTTagCompound());
        itemStack.getTagCompound().setInteger("uses", uses);
    }

    protected int getUses(ItemStack itemStack) {
        return itemStack.getTagCompound() == null ? 0 : itemStack.getTagCompound().getInteger("uses");
    }

    public int getMaxItemUseDuration(ItemStack p_77626_1_)
    {
        return 32;
    }

    public EnumAction getItemUseAction(ItemStack p_77661_1_) {
        return action;
    }

    public boolean onItemUse(ItemStack p_77648_1_, EntityPlayer p_77648_2_, World p_77648_3_, int p_77648_4_, int p_77648_5_, int p_77648_6_, int p_77648_7_, float p_77648_8_, float p_77648_9_, float p_77648_10_)
    {
        return false;
    }

    public ItemStack onItemRightClick(ItemStack p_77659_1_, World p_77659_2_, EntityPlayer p_77659_3_)
    {
        p_77659_3_.setItemInUse(p_77659_1_, this.getMaxItemUseDuration(p_77659_1_));
        return p_77659_1_;
    }

    @Override
    public ItemStack onEaten(ItemStack p_77654_1_, World p_77654_2_, EntityPlayer p_77654_3_) {
        int uses = getUses(p_77654_1_);
        if(uses < maxUses) {
            uses++;
            onUsed(p_77654_1_, p_77654_2_, p_77654_3_);
        }
        if(uses >= maxUses) {
            p_77654_1_.stackSize--;
            return p_77654_1_;
        }
        setUses(p_77654_1_, uses);
        return p_77654_1_;
    }

    public abstract void onUsed(ItemStack stack, World world, EntityPlayer player);
}
