package ru.xlv.mcdayz.item;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.util.ResourceLocation;
import ru.xlv.mcdayz.MCDayZ;
import ru.xlv.mcdayz.models.ModelBackpack;
import ru.xlv.mcdayz.utils.Utils;

public class ItemBackpack extends ItemBase {

    private int slotCount;

    @SideOnly(Side.CLIENT)
    private ResourceLocation texture;
    @SideOnly(Side.CLIENT)
    private ModelBackpack model;

    public ItemBackpack(String name, int slots, String modelClass) {
        super(name);
        slotCount = slots;
        setMaxStackSize(1);
        if(!Utils.isServerSide()) {
            texture = new ResourceLocation(MCDayZ.MODID, "textures/models/" + name + ".png");
            try {
                model = (ModelBackpack) Class.forName("ru.xlv.mcdayz.models." + modelClass).newInstance();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            }
        }
    }

    public int getSlotCount() {
        return slotCount;
    }

    @SideOnly(Side.CLIENT)
    public ResourceLocation getModelTexture() {
        return texture;
    }

    @SideOnly(Side.CLIENT)
    public ModelBackpack getModel() {
        return model;
    }
}
