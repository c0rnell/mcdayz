package ru.xlv.mcdayz.item;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumAction;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.CraftingManager;
import net.minecraft.world.World;
import ru.xlv.mcdayz.utils.ExtendedPlayer;
import ru.xlv.mcdayz.utils.MCDayZConfig;

import static ru.xlv.mcdayz.utils.MCDayZConfig.*;

public class Items {

    public static ItemBase tacticalBacon = new ItemUsable("tacticalBacon", 3, EnumAction.eat) {
        @Override
        public void onUsed(ItemStack stack, World world, EntityPlayer player) {
            ExtendedPlayer extendedPlayer = ExtendedPlayer.get(player);
            extendedPlayer.addFood(baconFoodHealing);
        }
    };
    public static ItemBase cereals = new ItemUsable("cereals", 2, EnumAction.eat) {
        @Override
        public void onUsed(ItemStack stack, World world, EntityPlayer player) {
            ExtendedPlayer extendedPlayer = ExtendedPlayer.get(player);
            extendedPlayer.addFood(cerealsFoodHealing);
        }
    };
    public static ItemBase sardines = new ItemUsable("sardines", 2, EnumAction.eat) {
        @Override
        public void onUsed(ItemStack stack, World world, EntityPlayer player) {
            ExtendedPlayer extendedPlayer = ExtendedPlayer.get(player);
            extendedPlayer.addFood(sardinesFoodHealing);
        }
    };
    public static ItemBase bobs = new ItemUsable("bobs", 2, EnumAction.eat) {
        @Override
        public void onUsed(ItemStack stack, World world, EntityPlayer player) {
            ExtendedPlayer extendedPlayer = ExtendedPlayer.get(player);
            extendedPlayer.addFood(bobsFoodHealing);
        }
    };
    public static ItemBase peaches = new ItemUsable("peaches", 1, EnumAction.eat) {
        @Override
        public void onUsed(ItemStack stack, World world, EntityPlayer player) {
            ExtendedPlayer extendedPlayer = ExtendedPlayer.get(player);
            extendedPlayer.addThirst(peachesThirstHealing);
        }
    };

    public static ItemBase militaryJar = new ItemUsable("militaryJar", 2, EnumAction.drink) {
        @Override
        public void onUsed(ItemStack stack, World world, EntityPlayer player) {
            ExtendedPlayer extendedPlayer = ExtendedPlayer.get(player);
            extendedPlayer.addThirst(jarThirstHealing);
        }
    };

    public static ItemBase pepsi = new ItemUsable("pepsi", 1, EnumAction.drink) {
        @Override
        public void onUsed(ItemStack stack, World world, EntityPlayer player) {
            ExtendedPlayer extendedPlayer = ExtendedPlayer.get(player);
            extendedPlayer.addThirst(pepsiThirstHealing);
        }
    };

    public static ItemBase mountieDew = new ItemUsable("mountieDew", 1, EnumAction.drink) {
        @Override
        public void onUsed(ItemStack stack, World world, EntityPlayer player) {
            ExtendedPlayer extendedPlayer = ExtendedPlayer.get(player);
            extendedPlayer.addThirst(mountiedewThirstHealing);
        }
    };

    public static ItemBase cocaCola = new ItemUsable("cocaCola", 1, EnumAction.drink) {
        @Override
        public void onUsed(ItemStack stack, World world, EntityPlayer player) {
            ExtendedPlayer extendedPlayer = ExtendedPlayer.get(player);
            extendedPlayer.addThirst(cocaThirstHealing);
        }
    };

    public static ItemBase waterBottle = new ItemUsable("waterBottle", 2, EnumAction.drink) {
        @Override
        public void onUsed(ItemStack stack, World world, EntityPlayer player) {
            ExtendedPlayer extendedPlayer = ExtendedPlayer.get(player);
            extendedPlayer.addThirst(wBottleThirstHealing);
        }
    };

    public static ItemBase bandage = new ItemUsableOnPlayer("bandage", 1) {
        @Override
        public void onUsed(ItemStack stack, World world, EntityPlayer player) {
            ExtendedPlayer extendedPlayer = ExtendedPlayer.get(player);
            extendedPlayer.hasBleeding = false;
        }

        @Override
        public void onUsedOnPlayer(EntityPlayer player, EntityPlayer target) {
            ExtendedPlayer extendedPlayer = ExtendedPlayer.get(target);
            extendedPlayer.hasBleeding = false;
        }

        @Override
        public ItemStack onItemRightClick(ItemStack p_77659_1_, World p_77659_2_, EntityPlayer p_77659_3_) {
            return p_77659_1_;
        }
    };

    public static ItemBase morphine = new ItemUsableOnPlayer("morphine", 1) {
        @Override
        public void onUsed(ItemStack stack, World world, EntityPlayer player) {
            ExtendedPlayer extendedPlayer = ExtendedPlayer.get(player);
            extendedPlayer.removeFracture();
        }

        @Override
        public void onUsedOnPlayer(EntityPlayer player, EntityPlayer target) {
            ExtendedPlayer extendedPlayer = ExtendedPlayer.get(target);
            extendedPlayer.removeFracture();
        }

        @Override
        public ItemStack onItemRightClick(ItemStack p_77659_1_, World p_77659_2_, EntityPlayer p_77659_3_) {
            return p_77659_1_;
        }
    };

    public static ItemBase antibiotics = new ItemUsable("antibiotics", 1, EnumAction.none) {
        @Override
        public void onUsed(ItemStack stack, World world, EntityPlayer player) {
            ExtendedPlayer extendedPlayer = ExtendedPlayer.get(player);
            if(extendedPlayer.infectionTimer >= MCDayZConfig.seriousInfectionTimeSec) return;
            extendedPlayer.hasInfection = false;
        }

        @Override
        public ItemStack onItemRightClick(ItemStack p_77659_1_, World p_77659_2_, EntityPlayer p_77659_3_) {
            return p_77659_1_;
        }
    };

    public static ItemBase smallAidKit = new ItemUsableOnPlayer("smallAidKit", 1) {
        @Override
        public void onUsed(ItemStack stack, World world, EntityPlayer player) {
            player.heal(smallAidKitHealing);
        }

        @Override
        public void onUsedOnPlayer(EntityPlayer player, EntityPlayer target) {
            target.heal(smallAidKitHealing);
        }

        @Override
        public ItemStack onItemRightClick(ItemStack p_77659_1_, World p_77659_2_, EntityPlayer p_77659_3_) {
            return p_77659_1_;
        }
    };

    public static ItemBase bigAidKit = new ItemUsableOnPlayer("bigAidKit", 1) {
        @Override
        public void onUsed(ItemStack stack, World world, EntityPlayer player) {
            player.heal(bigAidKitHealing);
        }

        @Override
        public void onUsedOnPlayer(EntityPlayer player, EntityPlayer target) {
            target.heal(bigAidKitHealing);
        }

        @Override
        public ItemStack onItemRightClick(ItemStack p_77659_1_, World p_77659_2_, EntityPlayer p_77659_3_) {
            return p_77659_1_;
        }
    };

    public static ItemBase backpack0 = new ItemBackpack("backpack0", 9, "ModelAlice");
    public static ItemBase backpack1 = new ItemBackpack("backpack1", 18, "ModelCoyote");
    public static ItemBase backpack2 = new ItemBackpack("backpack2", 27, "ModelCzech");

    public static ItemBase metal = new ItemBase("metal");
    public static ItemBase rope = new ItemBase("rope");
    public static ItemBase pin = new ItemBase("pin");
    public static ItemBase tent = new ItemBuilding("tent");
    public static ItemBase adrenaline = new ItemUsableOnPlayer("adrenaline", 1) {
        @Override
        protected void onUsedOnPlayer(EntityPlayer player, EntityPlayer target) {
            ExtendedPlayer extendedPlayer = ExtendedPlayer.get(target);
            extendedPlayer.removeUnconscious();
        }
    };

    public static ItemBase bloodBag = new ItemUsableOnPlayer("bloodBag", 1) {
        @Override
        protected void onUsedOnPlayer(EntityPlayer player, EntityPlayer target) {
            target.heal(MCDayZConfig.bloodBagHealing);
        }
    };
    public static ItemBase emptyBloodBag = new ItemBase("emptyBloodBag");

    public static void register() {
        bloodBag.enableUsing();
        regItem(bloodBag);
        regItem(emptyBloodBag);
        regItem(adrenaline);
        regItem(tent);
        regItem(metal);
        regItem(rope);
        regItem(pin);
        regItem(backpack0);
        regItem(backpack1);
        regItem(backpack2);
        smallAidKit.enableUsing();
        regItem(smallAidKit);
        bigAidKit.enableUsing();
        regItem(bigAidKit);
        antibiotics.enableUsing();
        regItem(antibiotics);
        morphine.enableUsing();
        regItem(morphine);
        bandage.enableUsing();
        bandage.setRecyclingResult(new ItemStack(net.minecraft.init.Items.leather));
        regItem(bandage);
        tacticalBacon.enableUsing();
        regItem(tacticalBacon);
        cereals.enableUsing();
        cereals.setRecyclingResult(new ItemStack(Items.metal));
        regItem(cereals);
        sardines.enableUsing();
        sardines.setRecyclingResult(new ItemStack(Items.metal));
        regItem(sardines);
        bobs.enableUsing();
        bobs.setRecyclingResult(new ItemStack(Items.metal));
        regItem(bobs);
        peaches.enableUsing();
        peaches.setRecyclingResult(new ItemStack(Items.metal));
        regItem(peaches);
        mountieDew.enableUsing();
        mountieDew.setRecyclingResult(new ItemStack(Items.metal));
        regItem(mountieDew);
        militaryJar.enableUsing();
        militaryJar.setRecyclingResult(new ItemStack(Items.metal, 2));
        regItem(militaryJar);
        pepsi.enableUsing();
        pepsi.setRecyclingResult(new ItemStack(Items.metal));
        regItem(pepsi);
        cocaCola.enableUsing();
        cocaCola.setRecyclingResult(new ItemStack(Items.metal));
        regItem(cocaCola);
        waterBottle.enableUsing();
        regItem(waterBottle);
        CraftingManager.getInstance().addRecipe(new ItemStack(Items.rope), "aaa", "aaa", "aaa", 'a', net.minecraft.init.Items.string);
        CraftingManager.getInstance().addRecipe(new ItemStack(Items.pin), "aas", "aas", "sas", 'a', metal);
        CraftingManager.getInstance().addRecipe(new ItemStack(Items.tent), "awa", "qwq", "awa", 'a', pin, 'q', rope, 'w', net.minecraft.init.Items.leather);
    }

    private static void regItem(Item item) {
        GameRegistry.registerItem(item, item.getUnlocalizedName());
    }
}
