package ru.xlv.mcdayz.item;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumAction;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public abstract class ItemUsableOnPlayer extends ItemUsable {

    ItemUsableOnPlayer(String name, int maxUses) {
        super(name, maxUses, EnumAction.none);
    }

    @Override
    public void onUsed(ItemStack stack, World world, EntityPlayer player) {}

    public void useOnPlayer(EntityPlayer player, EntityPlayer target) {
        int uses = getUses(player.inventory.getCurrentItem());
        if(uses < maxUses) {
            uses++;
            onUsedOnPlayer(player, target);
        }
        if(uses >= maxUses) {
            player.inventory.getCurrentItem().stackSize--;
            return;
        }
        setUses(player.inventory.getCurrentItem(), uses);
    }

    protected abstract void onUsedOnPlayer(EntityPlayer player, EntityPlayer target);
}
