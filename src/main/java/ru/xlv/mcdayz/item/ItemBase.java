package ru.xlv.mcdayz.item;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ChatComponentText;
import ru.xlv.mcdayz.MCDayZ;

public class ItemBase extends Item {

    private ItemStack[] recyclingResult;
    private boolean canBeUsed, canBeRecycled;

    ItemBase(String name) {
        setUnlocalizedName(name);
        setTextureName(MCDayZ.MODID + ":" + name);
        setCreativeTab(MCDayZ.tab);
    }

    public ItemBase setTab(CreativeTabs tab) {
        setCreativeTab(tab);
        return this;
    }

    void enableUsing() {
        canBeUsed = true;
    }

    public boolean canBeUsed() { return canBeUsed; }

    public boolean canBeRecycled() { return canBeRecycled; }

    public boolean hasMenu() {
        return canBeUsed || canBeRecycled;
    }

    public void tryRecycle(EntityPlayer player, ItemStack stack) {
        if(stack != null && stack.getItem() instanceof ItemBase && ((ItemBase) stack.getItem()).recyclingResult != null) {
            stack.stackSize--;
            for(ItemStack is : recyclingResult) {
                player.inventory.addItemStackToInventory(is);
            }
        } else {
            player.addChatMessage(new ChatComponentText("You can't recycle this item!"));
        }
    }

    void setRecyclingResult(ItemStack... itemStacks) {
        recyclingResult = itemStacks;
        canBeRecycled = true;
    }
}
