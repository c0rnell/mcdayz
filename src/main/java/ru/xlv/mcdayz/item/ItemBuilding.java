package ru.xlv.mcdayz.item;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;
import ru.xlv.mcdayz.entity.EntityTent;

public class ItemBuilding extends ItemBase {

    private EntityTent entityBuilding;
    private boolean spawned;

    public ItemBuilding(String name) {
        super(name);
    }

    @Override
    public boolean onItemUse(ItemStack is, EntityPlayer p, World w, int x, int y, int z, int p_77648_7_, float p_77648_8_, float p_77648_9_, float p_77648_10_) {
        if(w.isRemote) return true;
        EntityTent tent = new EntityTent(w);
        tent.setPositionAndRotation(x + .5, y + 1, z + .5, -p.rotationYaw, -p.rotationPitch);
        w.spawnEntityInWorld(tent);
        is.stackSize--;
        //        if(entityBuilding == null) {
//            entityBuilding = new EntityTent(w);
//            entityBuilding.addInventory(36);
//        }
//        entityBuilding.rotationYaw = -p.rotationYaw;
//        MovingObjectPosition movingObjectPosition = getMovingObjectPosition(p, 1F, 4);
//        if(movingObjectPosition == null || movingObjectPosition.hitVec == null) return false;
//        entityBuilding.setPositionAndUpdate(movingObjectPosition.hitVec.xCoord, movingObjectPosition.hitVec.yCoord, movingObjectPosition.hitVec.zCoord);
//        if(spawned) w.spawnEntityInWorld(entityBuilding);
//        spawned = true;
//        is.stackSize--;
        return true;
    }

//    private MovingObjectPosition getMovingObjectPosition(EntityLivingBase e, float fasc, double distance) {
//        Vec3 vec3 = Vec3.createVectorHelper(e.posX, e.posY + e.getEyeHeight(), e.posZ);
//        Vec3 vec31 = e.getLook(fasc);
//        Vec3 vec32 = vec3.addVector(vec31.xCoord * distance, vec31.yCoord * distance, vec31.zCoord * distance);
//        return e.worldObj.rayTraceBlocks(vec3, vec32, false);
//    }
}
