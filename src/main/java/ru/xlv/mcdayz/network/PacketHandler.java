package ru.xlv.mcdayz.network;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.network.FMLEventChannel;
import cpw.mods.fml.common.network.FMLNetworkEvent;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.network.internal.FMLProxyPacket;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import io.netty.buffer.ByteBufInputStream;
import io.netty.buffer.ByteBufOutputStream;
import io.netty.buffer.Unpooled;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.util.AxisAlignedBB;
import net.minecraftforge.common.MinecraftForge;
import ru.xlv.mcdayz.MCDayZ;
import ru.xlv.mcdayz.entity.EntityCorpse;
import ru.xlv.mcdayz.event.OpenPlayerInventoryServerEvent;
import ru.xlv.mcdayz.gui.GuiHandler;
import ru.xlv.mcdayz.gui.overlay.OverlayManager;
import ru.xlv.mcdayz.gui.overlay.OverlayProgressBar;
import ru.xlv.mcdayz.item.ItemBase;
import ru.xlv.mcdayz.item.ItemUsable;
import ru.xlv.mcdayz.item.ItemUsableOnPlayer;
import ru.xlv.mcdayz.utils.ExtendedPlayer;
import ru.xlv.mcdayz.utils.MCDayZConfig;
import ru.xlv.mcdayz.utils.Utils;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.Callable;

public class PacketHandler {

    private static FMLEventChannel channelUpdate;
    private static FMLEventChannel channelInv;
    private static FMLEventChannel channelOverlay;
    private static FMLEventChannel channelItemUse;
    private static FMLEventChannel channelItemRecycle;
    private static FMLEventChannel channelCorpseUpdate;
    private static FMLEventChannel channelCodeLocker;
    private static final String CHANNEL_UPDATE = "dzsUpd";
    private static final String CHANNEL_INV = "dzsInv";
    private static final String CHANNEL_OVERLAY = "dzsOvrl";
    private static final String CHANNEL_ITEM_USE = "dzsIUse";
    private static final String CHANNEL_ITEM_RECYCLE = "dzsIRec";
    private static final String CHANNEL_UPDATE_CORPSE = "dzsCUpd";
    private static final String CHANNEL_CODE_LOCKER = "dzsCodeL";

    public PacketHandler() {
        channelUpdate = registerNewChannel(this, CHANNEL_UPDATE);
        channelInv = registerNewChannel(this, CHANNEL_INV);
        channelOverlay = registerNewChannel(this, CHANNEL_OVERLAY);
        channelItemUse = registerNewChannel(this, CHANNEL_ITEM_USE);
        channelItemRecycle = registerNewChannel(this, CHANNEL_ITEM_RECYCLE);
        channelCorpseUpdate = registerNewChannel(this, CHANNEL_UPDATE_CORPSE);
        channelCodeLocker = registerNewChannel(this, CHANNEL_CODE_LOCKER);
    }

    @SubscribeEvent
    public void onPacket(FMLNetworkEvent.ServerCustomPacketEvent event) throws IOException {
        ByteBufInputStream bbis = new ByteBufInputStream(event.packet.payload());
        if (event.packet.channel().equals(CHANNEL_INV)) {
            String playerName = bbis.readUTF();
            EntityPlayer player = Utils.getPlayerByName(playerName);
            if (player != null) {
                OpenPlayerInventoryServerEvent openPlayerInventoryServerEvent = new OpenPlayerInventoryServerEvent(player);
                if(!MinecraftForge.EVENT_BUS.post(openPlayerInventoryServerEvent)) {
                    player.openGui(MCDayZ.instance, GuiHandler.GUI_PLAYER_INVENTORY_ID, player.worldObj,
                            (int) player.posX, (int) player.posY, (int) player.posZ);
                }
            }
        } else if(event.packet.channel().equals(CHANNEL_OVERLAY)) {
            int id = bbis.readInt();
            switch (id) {
                case OverlayManager.GUI_OVERLAY_PROGRESS_BAR_ID: {
                    int interactID = bbis.readInt();
                    switch (interactID) {
                        case OverlayProgressBar.INTERACT_PLAYER_ITEM_ID: {
                            String playerName = bbis.readUTF();
                            String targetName = bbis.readUTF();
                            int itemID = bbis.readInt();
                            Item item = Item.getItemById(itemID);
                            if (item instanceof ItemUsableOnPlayer) {
                                EntityPlayer player = Utils.getPlayerByName(playerName);
                                EntityPlayer target = Utils.getPlayerByName(targetName);
                                if (player == null || target == null) return;
                                ((ItemUsableOnPlayer) item).useOnPlayer(player, target);
                            }
                        }
                    }
                } break;
            }
        } else if(event.packet.channel().equals(CHANNEL_ITEM_USE)) {
            String playerName = bbis.readUTF();
            int slot = bbis.readInt();
            EntityPlayer player = Utils.getPlayerByName(playerName);
            if(player == null) return;
            if(player.inventory.getStackInSlot(slot) != null && player.inventory.getStackInSlot(slot).getItem() instanceof ItemUsable) {
                (player.inventory.getStackInSlot(slot).getItem()).onEaten(player.inventory.getStackInSlot(slot), player.worldObj, player);
                ((ItemUsable) player.inventory.getStackInSlot(slot).getItem()).onUsed(player.inventory.getStackInSlot(slot), player.worldObj, player);
            }
        } else if(event.packet.channel().equals(CHANNEL_ITEM_RECYCLE)) {
            String playerName = bbis.readUTF();
            int slot = bbis.readInt();
            EntityPlayer player = Utils.getPlayerByName(playerName);
            if(player == null) return;
            if(player.inventory.getStackInSlot(slot) != null && player.inventory.getStackInSlot(slot).getItem() instanceof ItemBase) {
                ((ItemBase) player.inventory.getStackInSlot(slot).getItem()).tryRecycle(player, player.inventory.getStackInSlot(slot));
            }
        }
        bbis.close();
    }

    @SideOnly(Side.CLIENT)
    @SubscribeEvent
    public void onPacket(FMLNetworkEvent.ClientCustomPacketEvent event) throws IOException {
        ByteBufInputStream bbis = new ByteBufInputStream(event.packet.payload());
        if(event.packet.channel().equals(CHANNEL_UPDATE)) {
            int t = bbis.readInt();
            int f = bbis.readInt();
            int mt = bbis.readInt();
            int mf = bbis.readInt();
            boolean b = bbis.readBoolean();
            boolean fr = bbis.readBoolean();
            boolean i = bbis.readBoolean();
            boolean u = bbis.readBoolean();
            int bt = bbis.readInt();
            int umt = bbis.readInt();
            int ust = bbis.readInt();
            int ubt = bbis.readInt();
            int rt = bbis.readInt();
            int at = bbis.readInt();
            MCDayZConfig.maxFood = mf;
            MCDayZConfig.maxThirst = mt;
            MCDayZConfig.recyclingTime = rt;
            MCDayZConfig.bandagingTime = bt;
            MCDayZConfig.usingBigAidKitTime = ubt;
            MCDayZConfig.usingMorphineTime = umt;
            MCDayZConfig.usingSmallAidKitTime = ust;
            MCDayZConfig.usingAntibioticsTime = at;
            ExtendedPlayer.get(Minecraft.getMinecraft().thePlayer).hasFracture = fr;
            ExtendedPlayer.get(Minecraft.getMinecraft().thePlayer).hasBleeding = b;
            ExtendedPlayer.get(Minecraft.getMinecraft().thePlayer).hasInfection = i;
            ExtendedPlayer.get(Minecraft.getMinecraft().thePlayer).isFaint = u;
            ExtendedPlayer.get(Minecraft.getMinecraft().thePlayer).food = f;
            ExtendedPlayer.get(Minecraft.getMinecraft().thePlayer).thirst = t;
            int pc = bbis.readInt();
            for(int j = 0; j < pc; j++) {
                MCDayZ.playerBackpacks.put(bbis.readUTF(), bbis.readInt());
            }
        } else if(event.packet.channel().equals(CHANNEL_OVERLAY)) {
            final int id = bbis.readInt();
            switch (id) {
                case OverlayManager.GUI_OVERLAY_NOTHING_ID:
                    OverlayManager.showOverlay(null);
                    break;
                case OverlayManager.GUI_OVERLAY_PROGRESS_BAR_ID: {
                    final String playerName = bbis.readUTF();
                    final String targetName = bbis.readUTF();
                    final int itemID = bbis.readInt();
                    int max = bbis.readInt();
                    OverlayProgressBar overlayElement = OverlayManager.getOverlayElement(id, max);
                    if(overlayElement == null) return;
                    overlayElement.setDestroyTask(new Callable() {
                        @Override
                        public Object call() {
                            sendPacketOverlayResult(id, OverlayProgressBar.INTERACT_PLAYER_ITEM_ID, playerName, targetName, itemID);
                            return null;
                        }
                    });
                    OverlayManager.showOverlay(overlayElement);
                } break;
            }
        } else if(event.packet.channel().equals(CHANNEL_UPDATE_CORPSE)) {
            String name = bbis.readUTF();
            double x = bbis.readDouble(), y = bbis.readDouble(), z = bbis.readDouble();
            List<EntityCorpse> list = Minecraft.getMinecraft().thePlayer.worldObj.getEntitiesWithinAABB(EntityCorpse.class,
                    AxisAlignedBB.getBoundingBox(x - .1, y - .1, z - .1, x + .1,y + .1, z + .1));
            if(list.isEmpty()) return;
            list.get(0).playerName = name;
        }
        bbis.close();
    }

    public static void sendPacketUpdateCorpse(EntityCorpse corpse) {
        if(!Utils.isServerSideOrSingleplayer()) return;
        ByteBufOutputStream bbos = new ByteBufOutputStream(Unpooled.buffer());
        try {
            bbos.writeUTF(corpse.playerName);
            bbos.writeDouble(corpse.posX);
            bbos.writeDouble(corpse.posY);
            bbos.writeDouble(corpse.posZ);
            FMLProxyPacket packet = new FMLProxyPacket(bbos.buffer(), CHANNEL_UPDATE_CORPSE);
            channelCorpseUpdate.sendToAllAround(packet, new NetworkRegistry.TargetPoint(corpse.dimension, corpse.posX, corpse.posY, corpse.posZ, 5));
            bbos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @SideOnly(Side.CLIENT)
    public static void sendPacketItemRecycle(int slotIndex) {
        ByteBufOutputStream bbos = new ByteBufOutputStream(Unpooled.buffer());
        try {
            bbos.writeUTF(Minecraft.getMinecraft().thePlayer.getCommandSenderName());
            bbos.writeInt(slotIndex);
            FMLProxyPacket packet = new FMLProxyPacket(bbos.buffer(), CHANNEL_ITEM_RECYCLE);
            channelItemRecycle.sendToServer(packet);
            bbos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @SideOnly(Side.CLIENT)
    public static void sendPacketItemUse(int slotIndex) {
        ByteBufOutputStream bbos = new ByteBufOutputStream(Unpooled.buffer());
        try {
            bbos.writeUTF(Minecraft.getMinecraft().thePlayer.getCommandSenderName());
            bbos.writeInt(slotIndex);
            FMLProxyPacket packet = new FMLProxyPacket(bbos.buffer(), CHANNEL_ITEM_USE);
            channelItemUse.sendToServer(packet);
            bbos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @SideOnly(Side.CLIENT)
    public static void sendPacketOverlayResult(int id, Object... params) {
        ByteBufOutputStream bbos = new ByteBufOutputStream(Unpooled.buffer());
        try {
            bbos.writeInt(id);
            for(Object o : params) {
                if (o instanceof String) bbos.writeUTF((String) o);
                else if(o instanceof Integer) bbos.writeInt((Integer) o);
            }
            FMLProxyPacket packet = new FMLProxyPacket(bbos.buffer(), CHANNEL_OVERLAY);
            channelOverlay.sendToServer(packet);
            bbos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void sendPacketOpenOverlayElement(EntityPlayer player, int id, Object... params) {
        if(!Utils.isServerSideOrSingleplayer()) return;
        ByteBufOutputStream bbos = new ByteBufOutputStream(Unpooled.buffer());
        try {
            bbos.writeInt(id);
            for(Object o : params) {
                if (o instanceof String) bbos.writeUTF((String) o);
                else if(o instanceof Integer) bbos.writeInt((Integer) o);
            }
            FMLProxyPacket packet = new FMLProxyPacket(bbos.buffer(), CHANNEL_OVERLAY);
            channelOverlay.sendTo(packet, (EntityPlayerMP) player);
            bbos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @SideOnly(Side.CLIENT)
    public static void sendPacketOpenInv() {
        ByteBufOutputStream bbos = new ByteBufOutputStream(Unpooled.buffer());
        try {
            bbos.writeUTF(Minecraft.getMinecraft().thePlayer.getCommandSenderName());
            FMLProxyPacket packet = new FMLProxyPacket(bbos.buffer(), CHANNEL_INV);
            channelInv.sendToServer(packet);
            bbos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void sendPacketUpdateTo(EntityPlayer player) {
        if(!Utils.isServerSide()) return;
        ByteBufOutputStream bbos = new ByteBufOutputStream(Unpooled.buffer());
        ExtendedPlayer extendedPlayer = ExtendedPlayer.get(player);
        try {
            bbos.writeInt(extendedPlayer.thirst);
            bbos.writeInt(extendedPlayer.food);
            bbos.writeInt(MCDayZConfig.maxThirst);
            bbos.writeInt(MCDayZConfig.maxFood);
            bbos.writeBoolean(extendedPlayer.hasBleeding);
            bbos.writeBoolean(extendedPlayer.hasFracture);
            bbos.writeBoolean(extendedPlayer.hasInfection);
            bbos.writeBoolean(extendedPlayer.isFaint);
            bbos.writeInt(MCDayZConfig.bandagingTime);
            bbos.writeInt(MCDayZConfig.usingMorphineTime);
            bbos.writeInt(MCDayZConfig.usingSmallAidKitTime);
            bbos.writeInt(MCDayZConfig.usingBigAidKitTime);
            bbos.writeInt(MCDayZConfig.recyclingTime);
            bbos.writeInt(MCDayZConfig.usingAntibioticsTime);
            List<EntityPlayer> list = Utils.getPlayersAround(player.posX, player.posY, player.posZ, 16);
            bbos.writeInt(list.size());
            for(EntityPlayer entityPlayer : list) {
                ExtendedPlayer extendedPlayer1 = ExtendedPlayer.get(entityPlayer);
                bbos.writeUTF(entityPlayer.getCommandSenderName());
                if(extendedPlayer1.inventory.getStackInSlot(0) != null)
                    bbos.writeInt(Item.getIdFromItem(extendedPlayer1.inventory.getStackInSlot(0).getItem()));
                else bbos.writeInt(0);
            }
            FMLProxyPacket packet = new FMLProxyPacket(bbos.buffer(), CHANNEL_UPDATE);
            channelUpdate.sendTo(packet, (EntityPlayerMP) player);
            bbos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private FMLEventChannel registerNewChannel(Object handler, String channelName){
        FMLEventChannel channel = NetworkRegistry.INSTANCE.newEventDrivenChannel(channelName);
        channel.register(handler);
        MinecraftForge.EVENT_BUS.register(handler);
        return channel;
    }
}
