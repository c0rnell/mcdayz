package ru.xlv.mcdayz.event;

import cpw.mods.fml.common.eventhandler.Cancelable;
import cpw.mods.fml.common.eventhandler.Event;
import net.minecraft.entity.player.EntityPlayer;

@Cancelable
public class OpenPlayerInventoryServerEvent extends Event {

    public EntityPlayer player;

    public OpenPlayerInventoryServerEvent(EntityPlayer player) {
        this.player = player;
    }
}
