package ru.xlv.mcdayz.models;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

/**
 * 123 - Undefined
 * Created using Tabula 4.1.1
 */
public class ModelTent extends ModelBase {
    public ModelRenderer Shape10;
    public ModelRenderer Shape9;
    public ModelRenderer Shape8;
    public ModelRenderer Shape7;
    public ModelRenderer Shape1;
    public ModelRenderer Shape2;
    public ModelRenderer Shape6;
    public ModelRenderer Shape5;
    public ModelRenderer Shape4;
    public ModelRenderer Shape3;
    public ModelRenderer shape3;
    public ModelRenderer shape3_1;

    public ModelTent() {
        this.textureWidth = 128;
        this.textureHeight = 64;
        this.Shape9 = new ModelRenderer(this, 0, 0);
        this.Shape9.setRotationPoint(-8.0F, 23.799999237060547F, 15.199999809265137F);
        this.Shape9.addBox(0.0F, 0.0F, 0.0F, 48, 1, 12, 0.0F);
        this.setRotateAngle(Shape9, 2.402461290359497F, -0.0F, 0.0F);
        this.Shape6 = new ModelRenderer(this, 0, 0);
        this.Shape6.setRotationPoint(-8.0F, 15.199999809265137F, -7.300000190734863F);
        this.Shape6.addBox(0.0F, 0.0F, 0.0F, 48, 1, 11, 0.0F);
        this.setRotateAngle(Shape6, 1.0814844369888306F, -0.0F, 0.0F);
        this.Shape7 = new ModelRenderer(this, 0, 0);
        this.Shape7.setRotationPoint(-8.0F, 6.199999809265137F, 1.600000023841858F);
        this.Shape7.addBox(0.0F, 0.0F, 0.0F, 48, 1, 6, 0.0F);
        this.setRotateAngle(Shape7, 1.93659257888794F, -0.0F, 0.0F);
        this.Shape3 = new ModelRenderer(this, 0, 0);
        this.Shape3.setRotationPoint(-7.800000190734863F, 20.0F, -15.399999618530273F);
        this.Shape3.addBox(0.0F, 0.0F, 0.0F, 1, 4, 1, 0.0F);
        this.Shape2 = new ModelRenderer(this, 0, 0);
        this.Shape2.setRotationPoint(38.79999923706055F, 20.0F, 15.0F);
        this.Shape2.addBox(0.0F, 0.0F, 0.0F, 1, 4, 1, 0.0F);
        this.shape3 = new ModelRenderer(this, 0, 10);
        this.shape3.setRotationPoint(-7.5F, 0.0F, -14.7F);
        this.shape3.addBox(0.0F, 0.0F, 0.0F, 1, 24, 30, 0.0F);
        this.Shape8 = new ModelRenderer(this, 0, 0);
        this.Shape8.setRotationPoint(-8.0F, 15.600000381469727F, 6.400000095367432F);
        this.Shape8.addBox(0.0F, 0.0F, 0.0F, 48, 1, 11, 0.0F);
        this.setRotateAngle(Shape8, 2.048128366470337F, -0.0F, 0.0F);
        this.Shape1 = new ModelRenderer(this, 0, 0);
        this.Shape1.setRotationPoint(-7.800000190734863F, 20.0F, 15.0F);
        this.Shape1.addBox(0.0F, 0.0F, 0.0F, 1, 4, 1, 0.0F);
        this.Shape10 = new ModelRenderer(this, 0, 0);
        this.Shape10.setRotationPoint(-8.0F, 23.799999237060547F, -15.199999809265137F);
        this.Shape10.addBox(0.0F, 0.0F, 0.0F, 48, 1, 12, 0.0F);
        this.setRotateAngle(Shape10, 0.8354860544204711F, -9.300982920179775E-17F, 3.100327529769176E-17F);
        this.shape3_1 = new ModelRenderer(this, 0, 10);
        this.shape3_1.setRotationPoint(38.5F, 0.0F, -14.7F);
        this.shape3_1.addBox(0.0F, 0.0F, 0.0F, 1, 24, 30, 0.0F);
        this.Shape5 = new ModelRenderer(this, 0, 0);
        this.Shape5.setRotationPoint(-8.0F, 6.0F, -2.200000047683716F);
        this.Shape5.addBox(0.0F, 0.0F, 0.0F, 48, 1, 6, 0.0F);
        this.setRotateAngle(Shape5, 1.2673776149749756F, -0.0F, 0.0F);
        this.Shape4 = new ModelRenderer(this, 0, 0);
        this.Shape4.setRotationPoint(38.79999923706055F, 20.0F, -15.399999618530273F);
        this.Shape4.addBox(0.0F, 0.0F, 0.0F, 1, 4, 1, 0.0F);
    }

    @Override
    public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
        setRotationAngles(f, f1, f2, f3, f4, f5, entity);
        this.Shape9.render(f5);
        this.Shape6.render(f5);
        this.Shape7.render(f5);
        this.Shape3.render(f5);
        this.Shape2.render(f5);
        this.shape3.render(f5);
        this.Shape8.render(f5);
        this.Shape1.render(f5);
        this.Shape10.render(f5);
        this.shape3_1.render(f5);
        this.Shape5.render(f5);
        this.Shape4.render(f5);
    }

    /**
     * This is a helper function from Tabula to set the rotation of model parts
     */
    public void setRotateAngle(ModelRenderer modelRenderer, float x, float y, float z) {
        modelRenderer.rotateAngleX = x;
        modelRenderer.rotateAngleY = y;
        modelRenderer.rotateAngleZ = z;
    }
}
