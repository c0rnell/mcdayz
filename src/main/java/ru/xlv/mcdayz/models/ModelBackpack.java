package ru.xlv.mcdayz.models;

import net.minecraft.client.model.ModelBiped;

public abstract class ModelBackpack extends ModelBiped {

    public abstract void render();
}
