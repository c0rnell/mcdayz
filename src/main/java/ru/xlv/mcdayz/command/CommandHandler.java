package ru.xlv.mcdayz.command;

import cpw.mods.fml.common.event.FMLServerStartingEvent;
import net.minecraft.command.ICommandSender;
import net.minecraft.util.ChatComponentText;
import ru.xlv.mcdayz.utils.PlayerSpawnData;

/**
 * Created by Xlv on 25.03.2018.
 */
public class CommandHandler {

    public static void register(FMLServerStartingEvent event){
        event.registerServerCommand(new Command("mcdayz") {
            @Override
            public void processCommand(ICommandSender sender, String[] strs) {
                if(strs.length > 4) {
                    if(strs[0].equals("addplayerspawn")) {
                        try {
                            int x = Integer.parseInt(strs[1]);
                            int y = Integer.parseInt(strs[2]);
                            int z = Integer.parseInt(strs[3]);
                            boolean b = Boolean.parseBoolean(strs[4]);
                            PlayerSpawnData.playerSpawnDataList.add(new PlayerSpawnData(x, y, z, b));
                            sender.addChatMessage(new ChatComponentText("Player spawn was successfully added!"));
                        } catch (NumberFormatException e) {
                            sender.addChatMessage(new ChatComponentText("Wrong params!"));
                        }
                    }
                }
            }
        });
    }
}
