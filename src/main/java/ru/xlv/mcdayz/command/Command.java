package ru.xlv.mcdayz.command;

import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;

/**
 * Created by Xlv on 07.11.2017.
 */
public abstract class Command extends CommandBase {

    private String name;
    private int level;

    public Command(String name){
        this.name = name;
    }

    public Command(String name, int lvl) {
        this(name);
        level = lvl;
    }

    @Override
    public int getRequiredPermissionLevel() {
        return level;
    }

    private boolean hasPermissions(ICommandSender sender) {
        if(sender instanceof EntityPlayer)
            return ((EntityPlayer) sender).capabilities.isCreativeMode;
        return false;
    }

    @Override
    public String getCommandName() {
        return name;
    }

    @Override
    public void processCommand(ICommandSender p_71515_1_, String[] p_71515_2_) {}

    @Override
    public String getCommandUsage(ICommandSender p_71518_1_) {
        return null;
    }
}
