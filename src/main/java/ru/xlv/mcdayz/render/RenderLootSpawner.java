package ru.xlv.mcdayz.render;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import ru.xlv.mcdayz.MCDayZ;
import ru.xlv.mcdayz.tileentity.TileEntityLootSpawner;

@SideOnly(Side.CLIENT)
public class RenderLootSpawner extends TileEntitySpecialRenderer {

    public void renderTileEntityAt(TileEntity tile, double x, double y, double z, float f) {
        this.render((TileEntityLootSpawner) tile, x, y, z, f);
    }

    private void render(TileEntityLootSpawner tile, double x, double y, double z, float f) {
        if(tile.inventory != null && tile.inventory.length > 0) {
            GL11.glPushMatrix();
            GL11.glTranslatef((float) x + .5F, (float) y + .15F, (float) z + .5F);
            if(Minecraft.getMinecraft().thePlayer.getDistance(tile.xCoord, tile.yCoord, tile.zCoord) < 8) {
                EntityItem item = new EntityItem(tile.getWorldObj());
                item.hoverStart = 0;
                for (int i = 0; i < tile.inventory.length; i++) {
                    if (tile.inventory[i] != null) {
                        GL11.glPushMatrix();
                        item.setEntityItemStack(tile.inventory[i]);
                        RenderManager.instance.renderEntityWithPosYaw(item, 0, 0, 0, 0, 0);
                        GL11.glPopMatrix();
                    }
                }
            }
            GL11.glPopMatrix();
        }
    }
}
