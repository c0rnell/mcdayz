package ru.xlv.mcdayz.render;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import ru.xlv.mcdayz.MCDayZ;
import ru.xlv.mcdayz.models.ModelTent;

@SideOnly(Side.CLIENT)
public class RenderTent extends RenderLiving {

    private ModelTent model = new ModelTent();
    private ResourceLocation tex = new ResourceLocation(MCDayZ.MODID, "textures/models/tent.png");

    public RenderTent(float p_i1262_2_) {
        super(new ModelBiped(), p_i1262_2_);
    }

    @Override
    public void doRender(Entity entity, double x, double y, double z, float yaw, float partialTick) {
        GL11.glPushMatrix();
        GL11.glTranslated((float)x, (float)y + 1.5, (float)z);
//        GL11.glRotatef(90, 1, 0, 0);
        GL11.glRotatef(180, 0, 0, 1);
        GL11.glRotatef(90 + entity.rotationYaw, 0, 1, 0);
        GL11.glTranslated(-1, 0, 0);
        Minecraft.getMinecraft().renderEngine.bindTexture(getEntityTexture(entity));
        model.render(entity, 0, 0, 0, 0, 0, 0.0625F);
        GL11.glPopMatrix();
    }

    @Override
    protected ResourceLocation getEntityTexture(Entity p_110775_1_) {
        return tex;
    }
}
