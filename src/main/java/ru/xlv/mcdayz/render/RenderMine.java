package ru.xlv.mcdayz.render;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.AdvancedModelLoader;
import net.minecraftforge.client.model.IModelCustom;
import org.lwjgl.opengl.GL11;
import ru.xlv.mcdayz.MCDayZ;

@SideOnly(Side.CLIENT)
public class RenderMine extends TileEntitySpecialRenderer {

	private IModelCustom model = AdvancedModelLoader.loadModel(new ResourceLocation(MCDayZ.MODID, "models/mine.obj"));
	private ResourceLocation tex = new ResourceLocation(MCDayZ.MODID, "textures/models/mine.png");

	@Override
	public void renderTileEntityAt(TileEntity tile, double x, double y, double z, float p_147500_8_) {
		GL11.glPushMatrix();
		GL11.glTranslated((float)x + .5, (float)y + 0.06F, (float)z + .5);
		GL11.glScalef(0.005F, 0.005F, 0.005F);
		Minecraft.getMinecraft().renderEngine.bindTexture(tex);
		model.renderAll();
		GL11.glPopMatrix();
	}
}
