package ru.xlv.mcdayz.render;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import ru.xlv.mcdayz.MCDayZ;
import ru.xlv.mcdayz.entity.EntityCorpse;

/**
 * Created by Xlv on 21.03.2018.
 */
@SideOnly(Side.CLIENT)
public class RenderCorpse extends RenderLiving {

    private static ResourceLocation tex = new ResourceLocation(MCDayZ.MODID, "textures/entities/corpse.png");
    private ModelBiped model = new ModelBiped();

    public RenderCorpse(float p_i1262_2_) {
        super(new ModelBiped(), p_i1262_2_);
    }

    @Override
    public void doRender(Entity entity, double x, double y, double z, float yaw, float partialTick) {
        EntityCorpse corpse = (EntityCorpse) entity;
        float scaleFactor = 0.0625F;
        GL11.glPushMatrix();
        GL11.glTranslated((float)x, (float)y + 0.14F, (float)z);
        GL11.glRotatef(90, 1, 0, 0);
        GL11.glRotatef(180, 0, 1, 0);
        GL11.glRotatef(entity.rotationYaw, 0, 0, 1);
        bindTexture(getEntityTexture(entity));
        model.setRotationAngles(0, 0, 0, 0, 0, 0, entity);
        model.bipedHead.render(scaleFactor);
        model.bipedRightArm.rotateAngleZ = 2.5F;
        model.bipedLeftLeg.rotateAngleZ = -0.3F;
        model.bipedLeftArm.rotateAngleZ = -0.5F;
        model.bipedRightLeg.rotateAngleZ = 0.1F;
        model.bipedLeftArm.render(scaleFactor);
        model.bipedRightArm.render(scaleFactor);
        model.bipedBody.render(scaleFactor);
        model.bipedLeftLeg.render(scaleFactor);
        model.bipedRightLeg.render(scaleFactor);
        GL11.glPopMatrix();
    }

    @Override
    protected ResourceLocation getEntityTexture(Entity p_110775_1_) {
        return tex;
    }
}
