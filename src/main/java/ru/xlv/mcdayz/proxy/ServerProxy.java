package ru.xlv.mcdayz.proxy;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import ru.xlv.mcdayz.TickHandler;
import ru.xlv.mcdayz.utils.MCDayZConfig;

public class ServerProxy extends CommonProxy {

    @Override
    public void preInit(FMLPreInitializationEvent event) {
        MCDayZConfig.loadServerConfig(event.getSuggestedConfigurationFile());
        FMLCommonHandler.instance().bus().register(new TickHandler());
    }
}
