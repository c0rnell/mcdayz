package ru.xlv.mcdayz.proxy;

import cpw.mods.fml.client.registry.ClientRegistry;
import cpw.mods.fml.client.registry.RenderingRegistry;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.common.MinecraftForge;
import ru.xlv.mcdayz.entity.EntityCorpse;
import ru.xlv.mcdayz.entity.EntityTent;
import ru.xlv.mcdayz.gui.overlay.RenderGameOverlay;
import ru.xlv.mcdayz.render.RenderCorpse;
import ru.xlv.mcdayz.render.RenderLootSpawner;
import ru.xlv.mcdayz.render.RenderMine;
import ru.xlv.mcdayz.render.RenderTent;
import ru.xlv.mcdayz.tileentity.TileEntityLootSpawner;
import ru.xlv.mcdayz.tileentity.TileEntityMine;
import ru.xlv.mcdayz.utils.MCDayZConfig;

public class ClientProxy extends CommonProxy {

    @Override
    public void preInit(FMLPreInitializationEvent event) {
        MCDayZConfig.loadClientConfig(event.getSuggestedConfigurationFile());
        MinecraftForge.EVENT_BUS.register(new RenderGameOverlay());
        RenderingRegistry.registerEntityRenderingHandler(EntityTent.class, new RenderTent(1));
        RenderingRegistry.registerEntityRenderingHandler(EntityCorpse.class, new RenderCorpse(1));
        ClientRegistry.bindTileEntitySpecialRenderer(TileEntityLootSpawner.class, new RenderLootSpawner());
        ClientRegistry.bindTileEntitySpecialRenderer(TileEntityMine.class, new RenderMine());
    }
}
