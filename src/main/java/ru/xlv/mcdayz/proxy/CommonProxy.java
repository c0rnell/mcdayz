package ru.xlv.mcdayz.proxy;

import cpw.mods.fml.common.event.FMLPreInitializationEvent;

public abstract class CommonProxy {

    public abstract void preInit(FMLPreInitializationEvent event);
}
