package ru.xlv.mcdayz;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiIngameMenu;
import net.minecraft.client.gui.inventory.GuiInventory;
import net.minecraft.entity.monster.EntityZombie;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ChatComponentText;
import net.minecraftforge.client.event.GuiOpenEvent;
import net.minecraftforge.client.event.RenderPlayerEvent;
import net.minecraftforge.event.entity.EntityEvent;
import net.minecraftforge.event.entity.living.LivingAttackEvent;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.event.entity.living.LivingFallEvent;
import net.minecraftforge.event.entity.player.EntityInteractEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import org.lwjgl.opengl.GL11;
import ru.xlv.mcdayz.entity.EntityCorpse;
import ru.xlv.mcdayz.gui.overlay.OverlayManager;
import ru.xlv.mcdayz.item.ItemBackpack;
import ru.xlv.mcdayz.item.ItemUsableOnPlayer;
import ru.xlv.mcdayz.item.Items;
import ru.xlv.mcdayz.network.PacketHandler;
import ru.xlv.mcdayz.utils.MCDayZDamageSource;
import ru.xlv.mcdayz.utils.ExtendedPlayer;
import ru.xlv.mcdayz.utils.MCDayZConfig;
import ru.xlv.mcdayz.utils.Utils;

import static ru.xlv.mcdayz.utils.MCDayZConfig.*;

public class Events {

    @SideOnly(Side.CLIENT)
    @SubscribeEvent
    public void event(PlayerEvent.LivingUpdateEvent event) {
        if(event.entity instanceof EntityPlayer) {
            if(OverlayManager.getCurrentOverlayElement() != null) {
                if(((EntityPlayer) event.entity).moveForward != 0 || ((EntityPlayer) event.entity).moveStrafing != 0) {
                    OverlayManager.showOverlay(null);
                }
            }
        }
    }

    @SubscribeEvent
    public void event(LivingDeathEvent event) {
        if(Utils.isServerSideOrSingleplayer() && event.entity instanceof EntityPlayer) {
            ExtendedPlayer extendedPlayer = ExtendedPlayer.get((EntityPlayer) event.entity);
            EntityCorpse entityCorpse = new EntityCorpse(event.entity.worldObj);
            entityCorpse.playerName = event.entity.getCommandSenderName();
            int k = 0;
            for(int i = 0; i < ((EntityPlayer) event.entity).inventory.mainInventory.length; i++) {
                if(((EntityPlayer) event.entity).inventory.getStackInSlot(i) == null) continue;
                ItemStack stack = ((EntityPlayer) event.entity).inventory.getStackInSlotOnClosing(i);
                entityCorpse.chest.setInventorySlotContents(k++, stack);
            }
            for(int i = 0; i < ((EntityPlayer) event.entity).inventory.armorInventory.length; i++) {
                if(((EntityPlayer) event.entity).inventory.armorInventory[i] == null) continue;
                ItemStack stack = ((EntityPlayer) event.entity).inventory.armorInventory[i];
                entityCorpse.chest.setInventorySlotContents(k++, stack);
            }
            for(int i = 0; i < extendedPlayer.inventory.inventory.length; i++) {
                if(extendedPlayer.inventory.getStackInSlot(i) == null) continue;
                ItemStack stack = extendedPlayer.inventory.getStackInSlotOnClosing(i);
                entityCorpse.chest.setInventorySlotContents(k++, stack);
            }
            entityCorpse.setPositionAndRotation(event.entity.posX, event.entity.posY, event.entity.posZ,
                    event.entity.rotationYaw, event.entity.rotationPitch);
            event.entity.worldObj.spawnEntityInWorld(entityCorpse);
        }
    }

    @SubscribeEvent
    public void onInteract(EntityInteractEvent event) {
        if(Utils.isServerSideOrSingleplayer()) {
            if(event.entity instanceof EntityPlayer) {
                if (event.target instanceof EntityPlayer) {
                    if (event.entityPlayer.inventory.getCurrentItem() != null) {
                        if (event.entityPlayer.inventory.getCurrentItem().getItem() instanceof ItemUsableOnPlayer) {
                            PacketHandler.sendPacketOpenOverlayElement(event.entityPlayer, OverlayManager.GUI_OVERLAY_PROGRESS_BAR_ID,
                                    event.entityPlayer.getCommandSenderName(), event.target.getCommandSenderName(),
                                    Item.getIdFromItem(event.entityPlayer.inventory.getCurrentItem().getItem()),
                                    Utils.getUsingTimeFor(event.entityPlayer.inventory.getCurrentItem().getItem()));
                            ExtendedPlayer.get(event.entityPlayer).startUsing(event.entityPlayer.inventory.getCurrentItem());
                            ExtendedPlayer.get((EntityPlayer) event.target).startUsing(null);
                        }
                    } else if (event.entityPlayer.isSneaking()) {
                        ExtendedPlayer extendedTarget = ExtendedPlayer.get((EntityPlayer) event.target);
                        float hp = ((EntityPlayer) event.target).getHealth();
                        event.entityPlayer.addChatMessage(new ChatComponentText("Player in " +
                                (hp > 15 ? "good" : hp > 10 ? "normal" : hp > 5 ? "bad" : "serious") + " condition"));
                        if (extendedTarget.isFaint)
                            event.entityPlayer.addChatMessage(new ChatComponentText("Player is unconscious."));
                        if (extendedTarget.hasBleeding)
                            event.entityPlayer.addChatMessage(new ChatComponentText("Player has bleeding."));
                        if (extendedTarget.hasFracture)
                            event.entityPlayer.addChatMessage(new ChatComponentText("Player has fracture."));
                        if (extendedTarget.hasInfection)
                            event.entityPlayer.addChatMessage(new ChatComponentText("Player has infection."));
                    } else {
                        ExtendedPlayer.get((EntityPlayer) event.target).openBackpackBy(event.entityPlayer);
                    }
                } else if(event.target instanceof EntityCorpse && event.entityPlayer.inventory.getCurrentItem() != null
                        && event.entityPlayer.inventory.getCurrentItem().getItem() == Items.emptyBloodBag) {
                    PacketHandler.sendPacketOpenOverlayElement(event.entityPlayer, OverlayManager.GUI_OVERLAY_PROGRESS_BAR_ID, MCDayZConfig.fillingBloodBagTime);
                }
            }
        }
    }

    @SideOnly(Side.CLIENT)
    @SubscribeEvent
    public void onRenderPlayer(RenderPlayerEvent.Specials.Pre event) {
        Utils.updatePlayerBackpack(event.entityPlayer);
        if(ExtendedPlayer.get(event.entityPlayer) != null) {
            ItemStack backpack = ExtendedPlayer.get(event.entityPlayer).inventory.getStackInSlot(0);

            if(backpack != null && backpack.getItem() instanceof ItemBackpack) {
                GL11.glPushMatrix();
                event.renderer.modelBipedMain.bipedBody.postRender(0.0625F);
                Minecraft.getMinecraft().renderEngine.bindTexture(((ItemBackpack) backpack.getItem()).getModelTexture());
                ((ItemBackpack) backpack.getItem()).getModel().render();
                GL11.glPopMatrix();
            }
        } else {
            ExtendedPlayer.register(event.entityPlayer);
        }
    }

    @SideOnly(Side.CLIENT)
    @SubscribeEvent
    public void event(GuiOpenEvent event) {
        if(event.gui != null && Minecraft.getMinecraft().thePlayer != null && ExtendedPlayer.get(Minecraft.getMinecraft().thePlayer) != null
                && ExtendedPlayer.get(Minecraft.getMinecraft().thePlayer).isFaint && !(event.gui instanceof GuiIngameMenu)) {
            event.setCanceled(true);
        }
        if (Minecraft.getMinecraft().thePlayer != null && event.gui instanceof GuiInventory
                && !Minecraft.getMinecraft().thePlayer.capabilities.isCreativeMode) {
            event.setCanceled(true);
            PacketHandler.sendPacketOpenInv();
        }
        OverlayManager.showOverlay(null);
    }

    @SideOnly(Side.CLIENT)
    @SubscribeEvent
    public void event1(LivingAttackEvent event) {
        if(event.entity instanceof EntityPlayer && OverlayManager.getCurrentOverlayElement() != null && !(event.source instanceof MCDayZDamageSource)) {
            OverlayManager.showOverlay(null);
        }
    }

    @SubscribeEvent
    public void event(LivingAttackEvent event) {
        if(!Utils.isServerSideOrSingleplayer()) return;
        if(event.entity instanceof EntityPlayer) {
            ExtendedPlayer extendedPlayer = ExtendedPlayer.get((EntityPlayer) event.entity);
            if(!extendedPlayer.isFaint && ((EntityPlayer) event.entity).getHealth() < 10) {
                if(event.entity.worldObj.rand.nextInt(100) < MCDayZConfig.faintChance) {
                    extendedPlayer.addFaint();
                }
            }
            if(event.source instanceof MCDayZDamageSource) return;
            if(event.entity.worldObj.rand.nextInt(100) < bleedingChance) {
                extendedPlayer.hasBleeding = true;
            }
            if(event.source.getEntity() instanceof EntityZombie) {
                if(event.entity.worldObj.rand.nextInt(100) < infectionChance) {
                    extendedPlayer.hasInfection = true;
                }
            }
        }
    }

    @SubscribeEvent
    public void event(LivingEvent.LivingJumpEvent event) {
        if(!Utils.isServerSideOrSingleplayer()) return;
        if(event.entity instanceof EntityPlayer && !((EntityPlayer) event.entity).capabilities.isCreativeMode) {
            ExtendedPlayer extendedPlayer = ExtendedPlayer.get((EntityPlayer) event.entity);
            extendedPlayer.addThirst(-MCDayZConfig.thirstForJump);
        }
    }

    @SubscribeEvent
    public void event(LivingFallEvent event) {
        if(!Utils.isServerSideOrSingleplayer()) return;
        if(event.entity instanceof EntityPlayer) {
            if(event.distance >= 3) {
                if(event.entity.worldObj.rand.nextInt(100) < fractureChance) {
                    ExtendedPlayer extendedPlayer = ExtendedPlayer.get((EntityPlayer) event.entity);
                    extendedPlayer.addFracture();
                }
            }
        }
    }

    @SubscribeEvent
    public void event(EntityEvent.EntityConstructing event) {
        if(event.entity instanceof EntityPlayer) {
            ExtendedPlayer.register((EntityPlayer) event.entity);
        }
    }
}
