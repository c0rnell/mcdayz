package ru.xlv.mcdayz.block;

import net.minecraft.block.ITileEntityProvider;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import ru.xlv.mcdayz.MCDayZ;
import ru.xlv.mcdayz.gui.GuiHandler;
import ru.xlv.mcdayz.tileentity.TileEntityLootSpawner;

public class BlockLootSpawner extends BlockBase implements ITileEntityProvider {

    public BlockLootSpawner(String name) {
        super(name);
    }

    @Override
    public boolean onBlockActivated(World w, int x, int y, int z, EntityPlayer p, int p_149727_6_, float p_149727_7_, float p_149727_8_, float p_149727_9_) {
        p.openGui(MCDayZ.instance, GuiHandler.GUI_CHEST_ID, w, x, y, z);
        return super.onBlockActivated(w, x, y, z, p, p_149727_6_, p_149727_7_, p_149727_8_, p_149727_9_);
    }

    public int getRenderType() {
        return -1;
    }

    public boolean isOpaqueCube() {
        return false;
    }

    public boolean renderAsNormalBlock() {
        return false;
    }

    @Override
    public TileEntity createNewTileEntity(World p_149915_1_, int p_149915_2_) {
        return new TileEntityLootSpawner();
    }
}
