package ru.xlv.mcdayz.block;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import ru.xlv.mcdayz.MCDayZ;

public class BlockBase extends Block {

    BlockBase(String name) {
        super(Material.rock);
        setCreativeTab(MCDayZ.tab);
        setBlockTextureName(MCDayZ.MODID + ":" + name);
        setBlockName(name);
    }

    public BlockBase setTab(CreativeTabs tab) {
        setCreativeTab(tab);
        return this;
    }
}
