package ru.xlv.mcdayz.block;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import ru.xlv.mcdayz.MCDayZ;
import ru.xlv.mcdayz.tileentity.TileEntityMine;

public class Blocks {

    public static Block itemSpawner = new BlockLootSpawner("itemSpawner").setTab(MCDayZ.tab_other);
    public static Block mine = new BlockTiled("mine", TileEntityMine.class, true);

    public static void register() {
        regBlock(itemSpawner);
        regBlock(mine);
    }

    private static void regBlock(Block block) {
        GameRegistry.registerBlock(block, block.getUnlocalizedName());
    }
}
