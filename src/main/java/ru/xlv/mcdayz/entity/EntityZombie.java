package ru.xlv.mcdayz.entity;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.World;

import java.util.List;

public class EntityZombie extends net.minecraft.entity.monster.EntityZombie {

    private EntityZombie leader;
    private boolean isLeader;

    public EntityZombie(World p_i1745_1_) {
        super(p_i1745_1_);
    }

    public void findLeader() {
        EntityZombie l = null;
        List<EntityZombie> list = worldObj.getEntitiesWithinAABB(EntityZombie.class,
                AxisAlignedBB.getBoundingBox(posX - 5, posY - 5, posZ - 5, posX + 5, posY + 5, posZ + 5));
        for(EntityZombie e : list) {
            if(e.isLeader) {
                l = e;
                break;
            }
        }
        if(l == null) {
            if(list.size() > 2) {
                l = list.get(worldObj.rand.nextInt(list.size()));
            }
        } else {
            l.isLeader = true;
        }
        leader = l;
    }

    @Override
    public void writeEntityToNBT(NBTTagCompound p_70014_1_) {
        super.writeEntityToNBT(p_70014_1_);
        p_70014_1_.setBoolean("isLeader", isLeader);
    }

    @Override
    public void readEntityFromNBT(NBTTagCompound p_70037_1_) {
        super.readEntityFromNBT(p_70037_1_);
        isLeader = p_70037_1_.getBoolean("isLeader");
    }

    public static void spawnFlockAt(World world, int x, int y, int z, int minCount, int maxCount) {
        int count = minCount + world.rand.nextInt(maxCount - minCount);
        int r = 5;
        EntityZombie leader = null;
        for(int i = 0; i < count; i++) {
            EntityZombie entityZombie = new EntityZombie(world);
            for(; y < world.getHeight(); y++) if(world.isAirBlock(x, y, z)) break;
            entityZombie.setPosition(x - r + world.rand.nextInt(r * 2), y, z - r + world.rand.nextInt(r * 2));
            if(i == 0) {
                leader = entityZombie;
                entityZombie.isLeader = true;
            } else {
                entityZombie.leader = leader;
            }
            world.spawnEntityInWorld(entityZombie);
        }
    }
}
