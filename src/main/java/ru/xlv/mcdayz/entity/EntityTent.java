package ru.xlv.mcdayz.entity;

import net.minecraft.entity.EntityAgeable;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.passive.EntityAnimal;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.AnimalChest;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.DamageSource;
import net.minecraft.world.World;
import ru.xlv.mcdayz.MCDayZ;
import ru.xlv.mcdayz.gui.GuiHandler;

public class EntityTent extends EntityAnimal {

    public AnimalChest chest = new AnimalChest("TentInv", 36);
    public int despawnTime;
    public String playerName = "Tent";
    private int time;

    public EntityTent(World par1World) {
        super(par1World);
        this.setSize(1F, 0.6F);
    }

    public EntityTent(World par1World, String s){
        super(par1World);
        this.playerName = s;
    }

    @Override
    public String getCommandSenderName() {
        return playerName;
    }

    public void addInventory(int i){
        chest = new AnimalChest("TentInv", i);
    }

    @Override
    public void onDeath(DamageSource p_70645_1_) {
        for(int i = 0; i < chest.getSizeInventory(); i++) {
            if(chest.getStackInSlot(i) != null)
                entityDropItem(chest.getStackInSlotOnClosing(i), 0);
        }
        super.onDeath(p_70645_1_);
    }

    @Override
    protected void applyEntityAttributes() {
        super.applyEntityAttributes();
        getAttributeMap().getAttributeInstance(SharedMonsterAttributes.maxHealth).setBaseValue(.1);
    }

    @Override
    public boolean interact(EntityPlayer par1EntityPlayer) {
        par1EntityPlayer.openGui(MCDayZ.instance, GuiHandler.GUI_BACKPACK_ID, par1EntityPlayer.worldObj, (int)posX, (int)posY, (int)posZ);
        return true;
    }

    @Override
    public void onLivingUpdate() {
//		super.onLivingUpdate();
    }

    @Override
    public void onEntityUpdate() {
        super.onEntityUpdate();
    }

    @Override
    protected String getHurtSound() {
        return null;
    }

    @Override
    public void readEntityFromNBT(NBTTagCompound par1nbtTagCompound) {
        super.readEntityFromNBT(par1nbtTagCompound);
        if(par1nbtTagCompound.hasKey("despawnTime"))
            despawnTime = par1nbtTagCompound.getInteger("despawnTime");
        else despawnTime = 60 * 30;

        if(par1nbtTagCompound.hasKey("playerName"))
            this.playerName = par1nbtTagCompound.getString("playerName");

        NBTTagList nbttaglist = par1nbtTagCompound.getTagList("Items", 10);

        for (int i = 0; i < nbttaglist.tagCount(); ++i)
        {
            NBTTagCompound nbttagcompound1 = nbttaglist.getCompoundTagAt(i);
            int j = nbttagcompound1.getByte("Slot") & 255;

            if (j >= 2 && j < this.chest.getSizeInventory())
            {
                this.chest.setInventorySlotContents(j, ItemStack.loadItemStackFromNBT(nbttagcompound1));
            }
        }
    }

    @Override
    public void writeEntityToNBT(NBTTagCompound par1nbtTagCompound) {
        super.writeEntityToNBT(par1nbtTagCompound);
        par1nbtTagCompound.setInteger("despawnTime", despawnTime);
        if(playerName != null)
            par1nbtTagCompound.setString("playerName", playerName);

        NBTTagList nbttaglist = new NBTTagList();

        for (int i = 2; i < this.chest.getSizeInventory(); ++i)
        {
            ItemStack itemstack = this.chest.getStackInSlot(i);

            if (itemstack != null)
            {
                NBTTagCompound nbttagcompound1 = new NBTTagCompound();
                nbttagcompound1.setByte("Slot", (byte)i);
                itemstack.writeToNBT(nbttagcompound1);
                nbttaglist.appendTag(nbttagcompound1);
            }
        }

        par1nbtTagCompound.setTag("Items", nbttaglist);
    }

    @Override
    public EntityAgeable createChild(EntityAgeable entityageable) {
        return null;
    }

}