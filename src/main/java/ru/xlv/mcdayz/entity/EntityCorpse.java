package ru.xlv.mcdayz.entity;

import net.minecraft.entity.EntityAgeable;
import net.minecraft.entity.passive.EntityAnimal;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.AnimalChest;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.world.World;
import ru.xlv.mcdayz.MCDayZ;
import ru.xlv.mcdayz.gui.GuiHandler;
import ru.xlv.mcdayz.network.PacketHandler;

/**
 * Created by Xlv on 21.03.2018.
 */
public class EntityCorpse extends EntityAnimal {

    public AnimalChest chest = new AnimalChest("CorpseInventory", 42);
    public String playerName = "Corpse";
    private int time;

    public EntityCorpse(World par1World) {
        super(par1World);
        this.setSize(1F, 0.6F);
    }

    @Override
    public String getCommandSenderName() {
        return playerName;
    }

    @Override
    public boolean interact(EntityPlayer par1EntityPlayer) {
        par1EntityPlayer.openGui(MCDayZ.instance, GuiHandler.GUI_BACKPACK_ID, worldObj, (int)posX, (int)posY, (int)posZ);
        return true;
    }

    @Override
    public void onLivingUpdate() {
        if(!worldObj.isRemote && ticksExisted % 20 * 12 == 0) {
            PacketHandler.sendPacketUpdateCorpse(this);
            time++;
            if(time > 3600) {
                setDead();
            }
        }

    }

    @Override
    protected String getHurtSound() {
        return null;
    }

    @Override
    public void readEntityFromNBT(NBTTagCompound par1nbtTagCompound) {
        super.readEntityFromNBT(par1nbtTagCompound);
        NBTTagList nbttaglist = par1nbtTagCompound.getTagList("Items", 10);
        time = par1nbtTagCompound.getInteger("time");
        playerName = par1nbtTagCompound.getString("playerName");

        for (int i = 0; i < nbttaglist.tagCount(); ++i)
        {
            NBTTagCompound nbttagcompound1 = nbttaglist.getCompoundTagAt(i);
            int j = nbttagcompound1.getByte("Slot") & 255;

            if (j >= 2 && j < this.chest.getSizeInventory())
            {
                this.chest.setInventorySlotContents(j, ItemStack.loadItemStackFromNBT(nbttagcompound1));
            }
        }
    }

    @Override
    public void writeEntityToNBT(NBTTagCompound par1nbtTagCompound) {
        super.writeEntityToNBT(par1nbtTagCompound);
        NBTTagList nbttaglist = new NBTTagList();
        par1nbtTagCompound.setInteger("time", time);
        par1nbtTagCompound.setString("playerName", playerName);

        for (int i = 2; i < this.chest.getSizeInventory(); ++i)
        {
            ItemStack itemstack = this.chest.getStackInSlot(i);

            if (itemstack != null)
            {
                NBTTagCompound nbttagcompound1 = new NBTTagCompound();
                nbttagcompound1.setByte("Slot", (byte)i);
                itemstack.writeToNBT(nbttagcompound1);
                nbttaglist.appendTag(nbttagcompound1);
            }
        }

        par1nbtTagCompound.setTag("Items", nbttaglist);
    }

    @Override
    public EntityAgeable createChild(EntityAgeable entityageable) {
        return null;
    }

}