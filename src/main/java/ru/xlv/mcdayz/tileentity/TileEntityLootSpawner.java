package ru.xlv.mcdayz.tileentity;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.tileentity.TileEntity;
import ru.xlv.mcdayz.utils.MCDayZConfig;
import ru.xlv.mcdayz.utils.Utils;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class TileEntityLootSpawner extends TileEntity implements IInventory {

    public static class ItemSpawnData {
        ItemStack stack;
        int chance;

        ItemSpawnData(String name, int c, int cc) {
            stack = new ItemStack(Utils.getItemByName(name), c);
            chance = cc;
        }
    }

    private static Random rand = new Random();

    private static List<ItemSpawnData> spawnDatas = new ArrayList<ItemSpawnData>();


    public ItemStack[] inventory;

    private int time;

    public TileEntityLootSpawner() {
        this.inventory = new ItemStack[this.getSizeInventory()];
        time = 0;
    }

    @Override
    public void readFromNBT(NBTTagCompound nbtTagCompound){
        super.readFromNBT(nbtTagCompound);

        NBTTagList tagList = nbtTagCompound.getTagList("Inventory", 10);
        for (int i = 0; i < tagList.tagCount(); i++){
            NBTTagCompound nbtTagCompound1 = (NBTTagCompound)tagList.getCompoundTagAt(i);
            byte slot = nbtTagCompound1.getByte("Slot");
            if(slot >= 0 && slot < inventory.length){
                inventory[slot] = ItemStack.loadItemStackFromNBT(nbtTagCompound1);
            }
        }

        time = nbtTagCompound.getInteger("time");
    }

    @Override
    public  void writeToNBT(NBTTagCompound nbtTagCompound){
        super.writeToNBT(nbtTagCompound);

        NBTTagList tagList = new NBTTagList();
        for(int i = 0; i < inventory.length; i++){
            ItemStack itemStack = inventory[i];
            if(itemStack != null){
                NBTTagCompound nbtTagCompound1 = new NBTTagCompound();
                nbtTagCompound1.setByte("Slot", (byte) i );
                itemStack.writeToNBT(nbtTagCompound1);
                tagList.appendTag(nbtTagCompound1);
            }
        }
        nbtTagCompound.setTag("Inventory", tagList);

        nbtTagCompound.setInteger("time", time);
    }

    @Override
    public void updateEntity() {
        if (time <= 0) {
            time = MCDayZConfig.lootSpawnTimeSec;
            clearInventory();
            for (int i = MCDayZConfig.minLootCount; i < MCDayZConfig.maxLootCount; i++) {
                if(MCDayZConfig.enableRandomLootCount && rand.nextBoolean()) continue;

                ItemStack stack = getRandomItem();
                if (stack != null) {
                    stack.setTagCompound(new NBTTagCompound());
                    setInventorySlotContents(i, stack);
                }
            }
        } else time--;
    }

    @Override
    public int getSizeInventory() {
        return 27;
    }

    @Override
    public ItemStack getStackInSlot(int index) {
        if (index < 0 || index >= this.getSizeInventory())
            return null;
        return this.inventory[index];
    }

    @Override
    public ItemStack decrStackSize(int index, int count) {
        if (this.getStackInSlot(index) != null) {
            ItemStack itemstack;

            if (this.getStackInSlot(index).stackSize <= count) {
                itemstack = this.getStackInSlot(index);
                this.setInventorySlotContents(index, null);
                this.markDirty();
                return itemstack;
            } else {
                itemstack = this.getStackInSlot(index).splitStack(count);

                if (this.getStackInSlot(index).stackSize <= 0) {
                    this.setInventorySlotContents(index, null);
                } else {
                    this.setInventorySlotContents(index, this.getStackInSlot(index));
                }

                this.markDirty();
                return itemstack;
            }
        } else {
            return null;
        }
    }

    @Override
    public ItemStack getStackInSlotOnClosing(int index) {
        ItemStack stack = this.getStackInSlot(index);
        this.setInventorySlotContents(index, null);
        return stack;
    }

    @Override
    public void setInventorySlotContents(int index, ItemStack stack) {
        if (index < 0 || index >= this.getSizeInventory())
            return;

        if (stack != null && stack.stackSize > this.getInventoryStackLimit())
            stack.stackSize = this.getInventoryStackLimit();

        if (stack != null && stack.stackSize == 0)
            stack = null;

        this.inventory[index] = stack;
        this.markDirty();
    }

    @Override
    public String getInventoryName() { return "Inventory"; }

    @Override
    public boolean hasCustomInventoryName() { return true; }

    @Override
    public int getInventoryStackLimit() { return 64; }

    @Override
    public boolean isUseableByPlayer(EntityPlayer player) {
        return worldObj.getTileEntity(xCoord, yCoord, zCoord) == this && player.getDistanceSq(xCoord + 0.5, yCoord + 0.5, zCoord + 0.5) < 64;
    }

    @Override
    public void closeInventory() {}

    @Override
    public boolean isItemValidForSlot(int p_94041_1_, ItemStack p_94041_2_) {
        return true;
    }

    @Override
    public void openInventory() {}

    public void clearInventory() {
        for (int i = 0; i < this.getSizeInventory(); i++) {
            this.inventory[i] = null;
        }
    }

    public static void addSpawnData(String unlocalizeName, int count, int chance) {
        spawnDatas.add(new ItemSpawnData(unlocalizeName, count, chance));
    }

    @Nullable
    public static ItemStack getRandomItem() {
        int r = rand.nextInt(100);
        ItemSpawnData temp = null;
        for(ItemSpawnData isd : spawnDatas) {
            if(isd.chance < r) {
                if(temp != null) {
                    if(isd.chance > temp.chance)
                        temp = isd;
                } else temp = isd;
            }
        }
        return temp != null ? temp.stack : null;
    }
}
