package ru.xlv.mcdayz.tileentity;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.DamageSource;
import ru.xlv.mcdayz.entity.EntityCorpse;

import java.util.List;
import java.util.Random;

public class TileEntityMine extends TileEntity {

    public TileEntityMine() {}

    @Override
    public void updateEntity() {
        super.updateEntity();
        AxisAlignedBB axis = AxisAlignedBB.getBoundingBox(this.xCoord, this.yCoord, this.zCoord,
                this.xCoord + 1, this.yCoord + 0.5F, this.zCoord + 1);
        List<EntityLivingBase> list = this.worldObj.getEntitiesWithinAABB(EntityLivingBase.class, axis);
        for(EntityLivingBase e : list){
            if(e != null && !e.isDead){
                onDeath(e);
                worldObj.setBlockToAir(xCoord, yCoord, zCoord);
                worldObj.removeTileEntity(xCoord, yCoord, zCoord);
                return;
            }
        }
    }

    private void onDeath(Entity entity) {
        Random r = new Random();
        double size = 4;
        double p = 3;
        for(double i = -size; i < size; i++)
            for(double k = 0; k < size; k++)
                for(double j = -size; j < size; j++){
                    this.worldObj.spawnParticle("largeexplode", this.xCoord + i/(size/p) + (-2 + r.nextInt(5)),
                            this.yCoord + 0.5D + k/(size/p) + (-2 + r.nextInt(5)), this.zCoord + j/(size/p) + (-2 + r.nextInt(5)), 0.0D, 0.0D, 0.0D);
                }
        AxisAlignedBB axis = AxisAlignedBB.getBoundingBox(this.xCoord - 2, this.yCoord - 2, this.zCoord - 2,
                this.xCoord + 2, this.yCoord + 2, this.zCoord + 2);
        List<Entity> list = this.worldObj.getEntitiesWithinAABB(Entity.class, axis);
        for(Entity e : list){
            if(e != null && !e.isDead && !(e instanceof EntityCorpse) && !(e instanceof EntityItem)){
                if(e instanceof EntityPlayer)
                    if(((EntityPlayer)e).capabilities.isCreativeMode)
                        continue;
                e.attackEntityFrom(DamageSource.outOfWorld, 50);
            }
        }
        this.worldObj.playSoundAtEntity(entity, "random.explode", 1, 1);
    }
}
