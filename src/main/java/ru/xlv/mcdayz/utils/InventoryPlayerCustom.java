package ru.xlv.mcdayz.utils;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;

public class InventoryPlayerCustom extends InventoryPlayer implements IInventory {

   public ItemStack[] inventory = new ItemStack[2];

   public InventoryPlayerCustom(EntityPlayer player) {
      super(player);
      this.player = player;
   }

   public void copy(InventoryPlayerCustom inv) {
      for(int i = 0; i < inv.getSizeInventory(); ++i) {
         ItemStack stack = inv.getStackInSlot(i);
         this.inventory[i] = stack == null?null:stack.copy();
      }

      this.markDirty();
   }

   public int getSizeInventory() {
      return this.inventory.length;
   }

   public ItemStack getStackInSlot(int slot) {
      return this.inventory[slot];
   }

   public ItemStack decrStackSize(int slot, int amount) {
      ItemStack stack = this.getStackInSlot(slot);
      if(stack != null) {
         if(stack.stackSize > amount) {
            stack = stack.splitStack(amount);
            this.markDirty();
         } else {
            this.setInventorySlotContents(slot, (ItemStack)null);
         }
      }

      return stack;
   }

   public ItemStack getStackInSlotOnClosing(int slot) {
      ItemStack stack = this.getStackInSlot(slot);
      this.setInventorySlotContents(slot, (ItemStack)null);
      return stack;
   }

   public void setInventorySlotContents(int slot, ItemStack stack) {
      this.inventory[slot] = stack;
      if(stack != null && stack.stackSize > this.getInventoryStackLimit()) {
         stack.stackSize = this.getInventoryStackLimit();
      }

      this.markDirty();
   }

   public String getInventoryName() {
      return "Storage";
   }

   public boolean hasCustomInventoryName() {
      return "Storage".length() > 0;
   }

   public int getInventoryStackLimit() {
      return 64;
   }

   public void markDirty() {
      for(int i = 0; i < this.getSizeInventory(); ++i) {
         if(this.getStackInSlot(i) != null && this.getStackInSlot(i).stackSize == 0) {
            this.inventory[i] = null;
         }
      }

   }

   public boolean isUseableByPlayer(EntityPlayer player) {
      return true;
   }

   public void openInventory() {}

   public void closeInventory() {}

   public boolean isItemValidForSlot(int slot, ItemStack stack) {
      return false;
   }

   public void writeToNBT(NBTTagCompound compound) {
      NBTTagList items = new NBTTagList();

      for(int i = 0; i < this.getSizeInventory(); ++i) {
         if(this.getStackInSlot(i) != null) {
            NBTTagCompound item = new NBTTagCompound();
            item.setByte("Slot", (byte)i);
            this.getStackInSlot(i).writeToNBT(item);
            items.appendTag(item);
         }
      }

      compound.setTag("Storage", items);
   }

   public void readFromNBT(NBTTagCompound compound) {
      NBTTagList items = compound.getTagList("Storage", compound.getId());

      for(int i = 0; i < items.tagCount(); ++i) {
         NBTTagCompound item = items.getCompoundTagAt(i);
         byte slot = item.getByte("Slot");
         if(slot >= 0 && slot < this.getSizeInventory()) {
            this.inventory[slot] = ItemStack.loadItemStackFromNBT(item);
         }
      }

   }
}
