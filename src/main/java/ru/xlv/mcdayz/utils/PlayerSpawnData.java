package ru.xlv.mcdayz.utils;

import java.util.ArrayList;
import java.util.List;

public class PlayerSpawnData {

    public static List<PlayerSpawnData> playerSpawnDataList = new ArrayList<PlayerSpawnData>();

    public int x, y, z;
    public boolean checkForPlayersAround;

    public PlayerSpawnData(int x, int y, int z, boolean flag) {
        this.x = x;
        this.y = y;
        this.z = z;
        checkForPlayersAround = flag;
    }
}
