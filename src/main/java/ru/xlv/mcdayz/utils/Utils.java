package ru.xlv.mcdayz.utils;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumChatFormatting;
import ru.xlv.mcdayz.MCDayZ;
import ru.xlv.mcdayz.item.Items;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Utils {

    public static boolean isServerSide() {
        try {
            Class.forName("net.minecraft.client.Minecraft");
        } catch (ClassNotFoundException e) {
            return true;
        }
        return false;
    }

    public static boolean isServerSideOrSingleplayer() {
        return isServerSide() || Minecraft.getMinecraft().isSingleplayer();
    }

    public static int getUsingTimeFor(Item item) {
        if(item == Items.bandage) return MCDayZConfig.bandagingTime;
        if(item == Items.smallAidKit) return MCDayZConfig.usingSmallAidKitTime;
        if(item == Items.morphine) return MCDayZConfig.usingMorphineTime;
        if(item == Items.bigAidKit) return MCDayZConfig.usingBigAidKitTime;
        if(item == Items.adrenaline) return MCDayZConfig.usingAdrenalineTime;
        if(item == Items.antibiotics) return MCDayZConfig.usingAntibioticsTime;
        return 400;
    }

    @Nullable
    public static Item getItemByName(String name) {
        for (Object anItemRegistry : Item.itemRegistry) {
            Item item = (Item) anItemRegistry;
            if (getItemName(item).equals(name)) {
                return item;
            }
        }
        return null;
    }

    private static String getItemName(Item item) {
        return item.getUnlocalizedName().replaceAll("item.", "");
    }

    static int getColor(EnumChatFormatting color) {
        int c = 0x7FFFFFFF;
        switch (color) {
            case RED: return 0x7FFF0000;
            case GREEN: return 0x7F00FF00;
            case BLUE: return 0x7F0000FF;
            case BLACK: return 0x7F000001;
            case WHITE: return 2130706433;
            case YELLOW: return 0x7FFFFF00;
            case GOLD: return 0x7FFFD700;
            case AQUA: return 0x7F00FFFF;
            case GRAY: return 0x7F808080;
        }
        return -c;
    }

    @SideOnly(Side.CLIENT)
    public static void updatePlayerBackpack(EntityPlayer player) {
        if(MCDayZ.playerBackpacks.containsKey(player.getCommandSenderName())) {
            if(MCDayZ.playerBackpacks.get(player.getCommandSenderName()) != 0) {
                ExtendedPlayer.get(player).inventory.setInventorySlotContents(0,
                        new ItemStack(Item.getItemById(MCDayZ.playerBackpacks.get(player.getCommandSenderName()))));
            } else {
                ExtendedPlayer.get(player).inventory.setInventorySlotContents(0, null);
            }
        } else {
            ExtendedPlayer.get(player).inventory.setInventorySlotContents(0, null);
        }
    }

    @Nullable
    public static EntityPlayer getPlayerByName(String name) {
        for(Object o : MinecraftServer.getServer().getConfigurationManager().playerEntityList) {
            if(name.equalsIgnoreCase(((EntityPlayer)o).getCommandSenderName())) return (EntityPlayer) o;
        }
        return null;
    }

    public static List<EntityPlayer> getPlayersAround(double x, double y, double z, int radius) {
        List<EntityPlayer> list = new ArrayList<EntityPlayer>();
        for(Object o : MinecraftServer.getServer().getConfigurationManager().playerEntityList) {
            if((((EntityPlayer)o)).getDistance(x, y, z) <= radius) list.add((EntityPlayer) o);
        }
        return list;
    }

}
