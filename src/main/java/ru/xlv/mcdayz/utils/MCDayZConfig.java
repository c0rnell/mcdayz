package ru.xlv.mcdayz.utils;

import net.minecraft.item.Item;
import net.minecraft.util.EnumChatFormatting;
import net.minecraftforge.common.config.Configuration;
import ru.xlv.mcdayz.tileentity.TileEntityLootSpawner;

import java.io.File;
import java.util.HashMap;

public class MCDayZConfig {

    public static int maxFood;
    public static int maxThirst;

    public static float bleedingDamage;
    public static float thirstDamage;
    public static float foodDamage;
    public static float infectionDamage;
    public static float fractureDamage;
    public static int bleedingChance;
    public static int fractureChance;
    public static int infectionChance;
    public static int thirstForJump;
    public static int faintChance;

    public static float bigAidKitHealing;
    public static float smallAidKitHealing;
    public static int baconFoodHealing;
    public static int cerealsFoodHealing;
    public static int sardinesFoodHealing;
    public static int bobsFoodHealing;
    public static int peachesThirstHealing;
    public static int jarThirstHealing;
    public static int pepsiThirstHealing;
    public static int mountiedewThirstHealing;
    public static int cocaThirstHealing;
    public static int wBottleThirstHealing;
    public static int bloodBagHealing;

    public static int seriousInfectionTimeSec;
    public static int bandagingTime;
    public static int fillingBloodBagTime;
    public static int usingAntibioticsTime;
    public static int usingMorphineTime;
    public static int usingSmallAidKitTime;
    public static int usingBigAidKitTime;
    public static int recyclingTime;
    public static int usingAdrenalineTime;
    public static int faintTimeSec;
    public static int lootSpawnTimeSec;
    public static int maxLootCount;
    public static int minLootCount;
    public static boolean enableRandomLootCount;
    public static boolean enableRandomPlayerSpawning;
    public static int playerSpawnRadiusOfCheckingForPlayers;

    public static HashMap<Integer, Integer> itemColors = new HashMap<Integer, Integer>();

    public static void loadClientConfig(File file) {
        Configuration config = new Configuration(file);
        config.load();
        boolean enableItemColors = config.get("settings", "enableItemColors", true).getBoolean();
        if(enableItemColors) {
            String c = "itemColors";
            for (String key : config.getCategory(c).keySet()) {
                itemColors.put(Item.getIdFromItem(Utils.getItemByName(key)),
                        Utils.getColor(EnumChatFormatting.getValueByName(config.getCategory(c).getValues().get(key).getString())));
            }
        }
        config.save();
    }

    public static void loadServerConfig(File file) {
        Configuration config = new Configuration(file);
        config.load();
        bleedingDamage = config.getFloat("bleedingDamage", "settings",.2F, 0, 100000, "");
        thirstDamage = config.getFloat("thirstDamage","settings", .2F, 0, 100000, "");
        foodDamage = config.getFloat("foodDamage","settings", .2F, 0, 100000, "");
        infectionDamage = config.getFloat("infectionDamage","settings", .2F, 0, 100000, "");
        fractureDamage = config.getFloat("fractureDamage","settings", .2F, 0, 100000, "");
        bleedingChance = config.get("settings", "bleedingChance", 60).getInt();
        fractureChance = config.get("settings", "fractureChance", 80).getInt();
        infectionChance = config.get("settings", "infectionChance", 50).getInt();
        bigAidKitHealing = config.getFloat("bigAidKitHealing","settings", 8, 0, 100000, "");
        smallAidKitHealing = config.getFloat("smallAidKitHealing","settings", 4, 0, 100000, "");
        baconFoodHealing = config.get("settings", "baconFoodHealing", 1080).getInt();
        cerealsFoodHealing = config.get("settings", "cerealsFoodHealing", 360).getInt();
        sardinesFoodHealing = config.get("settings", "sardinesFoodHealing", 480).getInt();
        bobsFoodHealing = config.get("settings", "bobsFoodHealing", 720).getInt();
        peachesThirstHealing = config.get("settings", "peachesThirstHealing", 180).getInt();
        jarThirstHealing = config.get("settings", "jarThirstHealing", 240).getInt();
        pepsiThirstHealing = config.get("settings", "pepsiThirstHealing", 120).getInt();
        mountiedewThirstHealing = config.get("settings", "mountiedewThirstHealing", 120).getInt();
        cocaThirstHealing = config.get("settings", "cocaThirstHealing", 120).getInt();
        wBottleThirstHealing = config.get("settings", "waterBottleThirstHealing", 120).getInt();
        bloodBagHealing = config.get("settings", "bloodBagHealing", 10).getInt();
        thirstForJump = config.get("settings", "thirstForJump", 80).getInt();
        maxFood = config.get("settings", "maxFood", 3600).getInt();
        maxThirst = config.get("settings", "maxThirst", 2400).getInt();
        seriousInfectionTimeSec = config.get("settings", "seriousInfectionTimeSec", 300).getInt();
        bandagingTime = config.get("settings", "bandagingTime", 5).getInt() * 20;
        fillingBloodBagTime = config.get("settings", "fillingBloodBagTime", 15).getInt() * 20;
        usingAntibioticsTime = config.get("settings", "usingAntibioticsTime", 7).getInt() * 20;
        usingMorphineTime = config.get("settings", "usingMorphineTime", 4).getInt() * 20;
        usingSmallAidKitTime = config.get("settings", "usingSmallAidKitTime", 7).getInt() * 20;
        usingBigAidKitTime = config.get("settings", "usingBigAidKitTime", 10).getInt() * 20;
        recyclingTime = config.get("settings", "recyclingTime", 5).getInt() * 20;
        usingAdrenalineTime = config.get("settings", "usingAdrenalineTime", 5).getInt() * 20;
        faintChance = config.get("settings", "faintChance", 25).getInt();
        faintTimeSec = config.get("settings", "faintTimeSec", 60).getInt();
        lootSpawnTimeSec = config.get("settings", "lootSpawnTimeSec", 300).getInt() * 20;
        maxLootCount = config.get("settings", "maxLootCount", 3).getInt();
        minLootCount = config.get("settings", "minLootCount", 1).getInt();
        enableRandomLootCount = config.get("settings", "enableRandomLootCount", true).getBoolean();
        enableRandomPlayerSpawning = config.get("settings", "enableRandomPlayerSpawning", true).getBoolean();
        playerSpawnRadiusOfCheckingForPlayers = config.get("settings", "playerSpawnRadiusOfCheckingForPlayers", 10).getInt();
        config.save();
        config = new Configuration(new File("config/mcdayzLootSpawn.cfg"));
        config.load();
        for(String k : config.getCategoryNames()) {
            TileEntityLootSpawner.addSpawnData(k, config.get(k, "count", 0).getInt(), config.get(k, "chance", 0).getInt());
        }
        config.save();
        config = new Configuration(new File("config/mcdayzPlayerSpawn.cfg"));
        config.load();
        for(String k : config.getCategoryNames()) {
            PlayerSpawnData.playerSpawnDataList.add(new PlayerSpawnData(config.get(k, "x", 0).getInt(),
                    config.get(k, "y", 0).getInt(), config.get(k, "z", 0).getInt(),
                    config.get(k, "check", false).getBoolean()));
        }
        config.save();
    }

    public static void saveServerConfig() {
        Configuration config = new Configuration(new File("config/mcdayzPlayerSpawn.cfg"));
        int i = 0;
        for(PlayerSpawnData psd : PlayerSpawnData.playerSpawnDataList) {
            config.get(i + "", "x", 0).set(psd.x);
            config.get(i + "", "y", 0).set(psd.y);
            config.get(i + "", "z", 0).set(psd.z);
            config.get(i++ + "", "check", false).set(psd.checkForPlayersAround);
        }
        config.save();
    }
}
