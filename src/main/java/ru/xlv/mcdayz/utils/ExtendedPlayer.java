package ru.xlv.mcdayz.utils;

import net.minecraft.entity.Entity;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import net.minecraftforge.common.IExtendedEntityProperties;
import ru.xlv.mcdayz.MCDayZ;
import ru.xlv.mcdayz.container.ContainerPlayerCustom;
import ru.xlv.mcdayz.gui.GuiHandler;
import ru.xlv.mcdayz.gui.overlay.OverlayManager;
import ru.xlv.mcdayz.network.PacketHandler;

public class ExtendedPlayer implements IExtendedEntityProperties {

    private final static String EXT_PROP_NAME = "ExtendedPlayer";

    public boolean hasBleeding;
    public boolean hasFracture;
    public boolean hasInfection;
    public boolean isFaint;
    public boolean isUsing;
    public boolean isBackpackOpened;

    public double usingX, usingY, usingZ;

    public int thirst = MCDayZConfig.maxThirst;
    public int food = MCDayZConfig.maxFood;
    public int faintTimer;
    public int infectionTimer;

    public EntityPlayer backpackUser;
    public ItemStack usingStack;
    private EntityPlayer player;
    public InventoryPlayerCustom inventory;

    private ExtendedPlayer(EntityPlayer player) {
        this.player = player;
        inventory = new InventoryPlayerCustom(player);
    }

    public void addFaint() {
        isFaint = true;
        player.getAttributeMap().getAttributeInstance(SharedMonsterAttributes.movementSpeed).setBaseValue(0.000001);
    }

    public void removeUnconscious() {
        isFaint = false;
        player.getAttributeMap().getAttributeInstance(SharedMonsterAttributes.movementSpeed).setBaseValue(0.10000000149011612D);
    }

    public void updateUsing() {
        if(!isUsing) return;
        if(player.inventory.getCurrentItem() != usingStack || player.posX != usingX || player.posY != usingY || player.posZ != usingZ)
            stopUsing();
    }

    public void startUsing(ItemStack stack) {
        usingStack = stack;
        usingX = player.posX;
        usingY = player.posY;
        usingZ = player.posZ;
        isUsing = true;
    }

    public void updateBackpackUsing() {
        if(!isBackpackOpened || backpackUser == null) return;
        if(player.getDistanceToEntity(backpackUser) > 3 || backpackUser.openContainer == null || player.openContainer instanceof ContainerPlayerCustom) {
            if(backpackUser.openContainer != null) backpackUser.closeScreen();
            backpackUser = null;
            isBackpackOpened = false;
        }
    }

    public void openBackpackBy(EntityPlayer player) {
        backpackUser = player;
        isBackpackOpened = true;
        player.openGui(MCDayZ.instance, GuiHandler.GUI_BACKPACK_ID, this.player.worldObj, (int) this.player.posX,
                (int) this.player.posY, (int) this.player.posZ);
    }

    public void stopUsing() {
        isUsing = false;
        usingStack = null;
        PacketHandler.sendPacketOpenOverlayElement(player, OverlayManager.GUI_OVERLAY_NOTHING_ID);
    }

    public void addFood(int f) {
        if(f < 0) {
            if (food + f >= 0)
                food += f;
            else
                food = 0;
        } else {
            if (food + f <= MCDayZConfig.maxFood)
                food += f;
            else
                food = MCDayZConfig.maxFood;
        }
    }

    public void addThirst(int t) {
        if(t < 0) {
            if (thirst + t >= 0)
                thirst += t;
            else
                thirst = 0;
        } else {
            if (thirst + t <= MCDayZConfig.maxThirst)
                thirst += t;
            else
                thirst = MCDayZConfig.maxThirst;
        }
    }

    public void addFracture() {
        hasFracture = true;
        player.getAttributeMap().getAttributeInstance(SharedMonsterAttributes.movementSpeed).setBaseValue(0.05);
    }

    public void removeFracture() {
        hasFracture = false;
        player.getAttributeMap().getAttributeInstance(SharedMonsterAttributes.movementSpeed).setBaseValue(0.10000000149011612D);
    }

    public static ExtendedPlayer get(EntityPlayer player) {
        return (ExtendedPlayer) player.getExtendedProperties(EXT_PROP_NAME);
    }

    public static void register(EntityPlayer player) {
        player.registerExtendedProperties(ExtendedPlayer.EXT_PROP_NAME, new ExtendedPlayer(player));
    }

    @Override
    public final void saveNBTData(NBTTagCompound compound) {
        NBTTagCompound properties = new NBTTagCompound();
        properties.setBoolean("hasBleeding", hasBleeding);
        properties.setBoolean("hasFracture", hasFracture);
        properties.setBoolean("hasInfection", hasInfection);
        properties.setBoolean("isFaint", isFaint);
        properties.setInteger("faintTimer", faintTimer);
        properties.setInteger("thirst", thirst);
        properties.setInteger("food", food);
        properties.setInteger("infectionTimer", infectionTimer);
        inventory.writeToNBT(compound);
        compound.setTag(EXT_PROP_NAME, properties);
    }

    @Override
    public final void loadNBTData(NBTTagCompound compound) {
        NBTTagCompound properties = (NBTTagCompound) compound.getTag(EXT_PROP_NAME);
        hasBleeding = properties.getBoolean("hasBleeding");
        hasFracture = properties.getBoolean("hasFracture");
        hasInfection = properties.getBoolean("hasInfection");
        isFaint = properties.getBoolean("isFaint");
        faintTimer = properties.getInteger("faintTimer");
        thirst = properties.getInteger("thirst");
        food = properties.getInteger("food");
        infectionTimer = properties.getInteger("infectionTimer");
        inventory.readFromNBT(compound);
    }

    @Override
    public void init(Entity entity, World world) {}
}
