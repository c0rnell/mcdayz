package ru.xlv.mcdayz.gui;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import ru.xlv.mcdayz.MCDayZ;
import ru.xlv.mcdayz.container.ContainerBackpack;
import ru.xlv.mcdayz.entity.EntityCorpse;
import ru.xlv.mcdayz.entity.EntityTent;
import ru.xlv.mcdayz.utils.ExtendedPlayer;

public class GuiBackpack extends GuiContainerCustom {

    private static final ResourceLocation texture = new ResourceLocation(MCDayZ.MODID, "textures/gui/backpack.png");
    private static final ResourceLocation texturePlayer = new ResourceLocation(MCDayZ.MODID, "textures/gui/backpackPlayer.png");
    private static final ResourceLocation arrows = new ResourceLocation(MCDayZ.MODID, "textures/gui/arrows.png");
    private Entity owner;
    private IInventory iInventory;

    public GuiBackpack(EntityPlayer player, IInventory par2IInventory, Entity par3EntityHorse) {
        super(new ContainerBackpack(player, par2IInventory, par3EntityHorse));
        this.owner = par3EntityHorse;
        if(par3EntityHorse instanceof EntityPlayer) iInventory = ((EntityPlayer) par3EntityHorse).inventory;
        else if(par3EntityHorse instanceof EntityCorpse) iInventory = ((EntityCorpse) par3EntityHorse).chest;
        else if(par3EntityHorse instanceof EntityTent) iInventory = ((EntityTent) par3EntityHorse).chest;
        this.allowUserInput = false;
        this.xSize = 512;
        this.ySize = 512;
    }

    protected void drawGuiContainerForegroundLayer(int par1, int par2) {
        this.drawCenteredString(mc.fontRenderer, owner.getCommandSenderName(), 115, 185, 0xffffff);
        this.drawCenteredString(mc.fontRenderer, mc.thePlayer.getCommandSenderName(), 276 + 115, 185, 0xffffff);
    }

    protected void drawGuiContainerBackgroundLayer(float par1, int par2, int par3) {
        GL11.glPushMatrix();
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        int k = (this.width - 256) / 2;
        int l = (this.height - 257) / 2;
        mc.getTextureManager().bindTexture(texture);
        this.drawTexturedModalRect(k - 100, l, 0, 0, 170, 256);
        mc.getTextureManager().bindTexture(texturePlayer);
        this.drawTexturedModalRect(k + 176, l, 0, 0, 256, 256);
//        if(ExtendedPlayer.get(Minecraft.getMinecraft().thePlayer).inventory.getStackInSlot(0) == null)
//            DrawHelper.drawBackpackIcon(this, k + 116, l + 26);
        mc.getTextureManager().bindTexture(arrows);
        this.drawTexturedModalRect(k + 83, l + 55, 0, 0, 80, 256);
        GL11.glPopMatrix();
    }
}
