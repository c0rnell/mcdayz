package ru.xlv.mcdayz.gui.overlay;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

import javax.annotation.Nullable;

public class OverlayManager {

    public static final int GUI_OVERLAY_NOTHING_ID = -1;
    public static final int GUI_OVERLAY_PROGRESS_BAR_ID = 0;

    @SideOnly(Side.CLIENT)
    private static IOverlayElement currentOverlayElement;

    @Nullable
    @SideOnly(Side.CLIENT)
    public static OverlayProgressBar getOverlayElement(int id, Object... o) {
        switch (id) {
            case GUI_OVERLAY_PROGRESS_BAR_ID: return new OverlayProgressBar((Integer) o[0]);
        }
        return null;
    }

    @SideOnly(Side.CLIENT)
    public static IOverlayElement getCurrentOverlayElement() { return currentOverlayElement; }

    @SideOnly(Side.CLIENT)
    public static void showOverlay(IOverlayElement element) {
        currentOverlayElement = element;
    }
}
