package ru.xlv.mcdayz.gui.overlay;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;
import ru.xlv.mcdayz.MCDayZ;

@SideOnly(Side.CLIENT)
public interface IOverlayElement {

    Minecraft mc = Minecraft.getMinecraft();
    ResourceLocation white = new ResourceLocation(MCDayZ.MODID, "textures/gui/white.png");

    void draw(int width, int height, int mx, int my);

}
