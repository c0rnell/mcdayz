package ru.xlv.mcdayz.gui.overlay;

import net.minecraft.client.Minecraft;
import org.lwjgl.opengl.GL11;
import ru.xlv.mcdayz.gui.DrawHelper;

import java.util.ArrayList;
import java.util.List;

public class OverlayListedMenu implements IOverlayElement {

    class PlayerMenuElement {

        SimpleTask task;
        String name;
        public boolean isMouseOverElement;

        PlayerMenuElement(String name, SimpleTask c) {
            task = c;
            this.name = name;
        }

        void draw(Minecraft mc, int x, int y, int w, int h) {
            if(!isMouseOverElement) GL11.glColor3d(.25, .5, .25);
            else GL11.glColor3d(.5, .75, .5);
            mc.getTextureManager().bindTexture(white);
            DrawHelper.drawCustom(x, w, h, y);
            mc.fontRenderer.drawString(name, x + w / 2 - mc.fontRenderer.getStringWidth(name) / 2, y + h / 2 - 4, 0xffffff);
            GL11.glColor3d(1, 1, 1);
        }

        void click() {
            try {
                task.execute();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public int x, y, xSize, ySize;
    public List<PlayerMenuElement> elements = new ArrayList<PlayerMenuElement>();

    public OverlayListedMenu(int x, int y) {
        this.x = x;
        this.y = y;
        xSize = 100;
    }

    public void addMenuElement(String name, SimpleTask callable) {
        elements.add(new PlayerMenuElement(name, callable));
        ySize += 10;
    }

    @Override
    public void draw(int width, int height, int mx, int my) {
        for(int i = 0; i < elements.size(); i++) {
            elements.get(i).draw(mc, x, y + 10 * i, xSize, 10);
        }
        updateElements(mx, my);
    }

    public void click(int x, int y) {
        int index = (y - this.y) / 10;
        if(index >= elements.size() || index < 0) return;
        if(x < this.x || x > this.x + xSize) return;
        PlayerMenuElement e = elements.get(index);
        e.click();
    }

    private void updateElements(int x, int y) {
        int index = (y - this.y) / 10;
        if(index >= elements.size() || index < 0) return;
        if(x < this.x || x > this.x + xSize) return;
        for(PlayerMenuElement e : elements)
            e.isMouseOverElement = false;
        elements.get(index).isMouseOverElement = true;
    }
}
