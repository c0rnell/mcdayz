package ru.xlv.mcdayz.gui.overlay;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.resources.I18n;
import ru.xlv.mcdayz.gui.DrawHelper;

import static org.lwjgl.opengl.GL11.*;

@SideOnly(Side.CLIENT)
public class OverlayProgressBar implements IOverlayElement {

    public static final int INTERACT_PLAYER_ITEM_ID = 0;

    private String string = I18n.format("gui.overlay.progressBarText");
    private int curPoint;
    private int maxPoint;

    private SimpleTask destroyTask;

    public OverlayProgressBar(int max) {
        maxPoint = max;
    }

    public void setDestroyTask(SimpleTask c) {
        destroyTask = c;
    }

    public void setText(String s) {
        string = s;
    }

    @Override
    public void draw(int width, int height, int mx, int my) {
        if(curPoint >= maxPoint) destroy();
        else curPoint++;
        glPushMatrix();
        glEnable(GL_ALPHA_TEST);
        mc.getTextureManager().bindTexture(white);
        glColor4f(.4F, .4F, .4F, .5F);
        DrawHelper.drawCustom(width / 2 - 100, 200, 20, height / 2 - 10);
        glColor4f(.2F, .2F, .2F, .5F);
        DrawHelper.drawCustom(width / 2 - 95, (int) (190F / maxPoint * curPoint), 16, height / 2 - 8);
        mc.fontRenderer.drawString(string, width / 2 - mc.fontRenderer.getStringWidth(string) / 2, height / 2 - 20, 0xffffff);
        String s = (int) (100F / maxPoint * curPoint) + "%";
        mc.fontRenderer.drawString(s, width / 2 - mc.fontRenderer.getStringWidth(s) / 2, height / 2 - 4, 0xffffff);
        glPopMatrix();
    }

    public void destroy() {
        if(destroyTask != null) {
            try {
                destroyTask.execute();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        OverlayManager.showOverlay(null);
    }
}
