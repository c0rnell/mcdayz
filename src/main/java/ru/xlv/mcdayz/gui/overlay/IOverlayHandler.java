package ru.xlv.mcdayz.gui.overlay;

/**
 * interface for Gui
 * */
public interface IOverlayHandler {

    void showOverlay(int x, int y);
    void closeOverlay();
}
