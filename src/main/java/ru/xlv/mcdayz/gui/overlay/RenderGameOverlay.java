package ru.xlv.mcdayz.gui.overlay;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.I18n;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.GuiIngameForge;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.common.MinecraftForge;
import org.lwjgl.opengl.GL11;
import ru.xlv.mcdayz.MCDayZ;
import ru.xlv.mcdayz.event.RenderStatsEvent;
import ru.xlv.mcdayz.gui.DrawHelper;
import ru.xlv.mcdayz.utils.ExtendedPlayer;
import ru.xlv.mcdayz.utils.MCDayZConfig;

@SideOnly(Side.CLIENT)
public class RenderGameOverlay extends GuiIngameForge {

    private ResourceLocation texture = new ResourceLocation(MCDayZ.MODID, "textures/gui/icons.png");

    public static boolean hasInfection;
    private static boolean preUnconscious;
    private static float uncScreenAlpha;

    public RenderGameOverlay() {
        super(Minecraft.getMinecraft());
    }

    @SubscribeEvent
    public void event(RenderGameOverlayEvent event) {
        if(event.type == RenderGameOverlayEvent.ElementType.FOOD) event.setCanceled(true);
        if(event.type == RenderGameOverlayEvent.ElementType.ALL) {
            renderStats(event.resolution.getScaledWidth(), event.resolution.getScaledHeight());
            render(event.resolution.getScaledWidth(), event.resolution.getScaledHeight());
        }
        if(ExtendedPlayer.get(mc.thePlayer).isFaint) {
            if(event.type == RenderGameOverlayEvent.ElementType.CHAT || event.type == RenderGameOverlayEvent.ElementType.PLAYER_LIST
                    || event.type == RenderGameOverlayEvent.ElementType.EXPERIENCE) {
                event.setCanceled(true);
            }
        }
    }

    private void renderStats(int w, int h) {
        if(MinecraftForge.EVENT_BUS.post(new RenderStatsEvent())) return;
        mc.getTextureManager().bindTexture(texture);
        drawTexturedModalRect(w / 2 + 92, h - 10, 0, 0, 9, 9);
        drawTexturedModalRect(w / 2 + 92, h - 20, 9, 0, 7, 9);
        int xf = 0;
        if(ExtendedPlayer.get(mc.thePlayer).hasBleeding) {
            drawTexturedModalRect(w / 2 + 92, h - 32, 16, 0, 7, 9);
            xf += 10;
        }
        if(ExtendedPlayer.get(mc.thePlayer).hasFracture) {
            drawTexturedModalRect(w / 2 + 92 + xf, h - 32, 30, 0, 7, 9);
            xf += 10;
        }
        if(hasInfection) {
            drawTexturedModalRect(w / 2 + 92 + xf, h - 32, 23, 0, 7, 9);
        }
        drawString(mc.fontRenderer, (int)(100F / MCDayZConfig.maxFood
                * ExtendedPlayer.get(mc.thePlayer).food) + "%", w / 2 + 101, h - 10, 0xffffff);
        drawString(mc.fontRenderer, (int)(100F / MCDayZConfig.maxThirst
                * ExtendedPlayer.get(mc.thePlayer).thirst) + "%", w / 2 + 101, h - 20, 0xffffff);
    }

    private void render(int w, int h) {
        GL11.glPushMatrix();
        ExtendedPlayer extendedPlayer = ExtendedPlayer.get(mc.thePlayer);
        if(OverlayManager.getCurrentOverlayElement() != null) OverlayManager.getCurrentOverlayElement().draw(w, h, 0, 0);
        if (preUnconscious != extendedPlayer.isFaint) {
            preUnconscious = extendedPlayer.isFaint;
        }

        if (extendedPlayer.isFaint) {
            if (uncScreenAlpha < 1) {
                uncScreenAlpha += 0.05F;
                if (uncScreenAlpha >= 1) uncScreenAlpha = 1;
            }
        } else {
            if (uncScreenAlpha > 0) {
                uncScreenAlpha -= 0.05F;
                if (uncScreenAlpha <= 0) {
                    uncScreenAlpha = 0;
                }
            }
        }
        GL11.glEnable(GL11.GL_BLEND);
        GL11.glEnable(GL11.GL_ALPHA_TEST);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        GL11.glColor4f(0, 0, 0, uncScreenAlpha);
        mc.getTextureManager().bindTexture(DrawHelper.white);
        DrawHelper.drawCustom(0, w, h, 0);
        if (extendedPlayer.isFaint)
            drawCenteredString(mc.fontRenderer, I18n.format("gui.inGame.unconscious"), w / 2, h / 2, 0xffffff);
        GL11.glColor4f(1, 1, 1, 1);
        GL11.glDisable(GL11.GL_BLEND);
        GL11.glDisable(GL11.GL_ALPHA_TEST);
        GL11.glPopMatrix();
    }
}
