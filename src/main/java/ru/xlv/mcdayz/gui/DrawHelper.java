package ru.xlv.mcdayz.gui;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import ru.xlv.mcdayz.MCDayZ;

@SideOnly(Side.CLIENT)
public class DrawHelper {

    public static ResourceLocation white = new ResourceLocation(MCDayZ.MODID, "textures/gui/white.png");
    public static ResourceLocation icons = new ResourceLocation(MCDayZ.MODID, "textures/gui/icons.png");

    public static void drawCustom(int x, int width, int height, int y) {
        Tessellator tessellator = Tessellator.instance;
        GL11.glEnable(GL11.GL_BLEND);
        OpenGlHelper.glBlendFunc(770, 771, 1, 0);
        tessellator.startDrawingQuads();
        tessellator.addVertexWithUV(x, y + height, 0.0D, 0, 1);
        tessellator.addVertexWithUV(x + width, y + height, 0.0D, 1, 1);
        tessellator.addVertexWithUV(x + width, y, 0.0D, 1, 0);
        tessellator.addVertexWithUV(x, y, 0.0D, 0, 0);
        tessellator.draw();
        GL11.glDisable(GL11.GL_BLEND);
    }

    public static void drawBackpackIcon(GuiScreen screen, int x, int y) {
        Minecraft.getMinecraft().getTextureManager().bindTexture(icons);
        screen.drawTexturedModalRect(x, y, 238, 0, 18, 18);
    }
}
