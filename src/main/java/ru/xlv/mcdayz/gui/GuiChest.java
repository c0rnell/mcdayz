package ru.xlv.mcdayz.gui;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;
import ru.xlv.mcdayz.MCDayZ;
import ru.xlv.mcdayz.container.ContainerChest;
import ru.xlv.mcdayz.tileentity.TileEntityLootSpawner;

public class GuiChest extends GuiContainerCustom {

    private static ResourceLocation tex = new ResourceLocation(MCDayZ.MODID, "textures/gui/chest.png");

    public GuiChest(EntityPlayer player, TileEntityLootSpawner tile) {
        super(new ContainerChest(player, tile));
    }

    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
        this.mc.getTextureManager().bindTexture(tex);
        this.drawTexturedModalRect(this.guiLeft, this.guiTop, 0, 0, this.xSize, this.ySize);
    }
}
