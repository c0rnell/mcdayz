package ru.xlv.mcdayz.gui;

import cpw.mods.fml.common.network.IGuiHandler;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.World;
import ru.xlv.mcdayz.MCDayZ;
import ru.xlv.mcdayz.container.ContainerBackpack;
import ru.xlv.mcdayz.container.ContainerChest;
import ru.xlv.mcdayz.container.ContainerPlayerCustom;
import ru.xlv.mcdayz.tileentity.TileEntityLootSpawner;
import ru.xlv.mcdayz.utils.ExtendedPlayer;

import java.util.List;

public class GuiHandler implements IGuiHandler {

    public static final int GUI_NOTHING_ID = -1;
    public static final int GUI_PLAYER_INVENTORY_ID = 0;
    public static final int GUI_BACKPACK_ID = 1;
    public static final int GUI_CHEST_ID = 2;

    @Override
    public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
        switch (ID) {
            case GUI_NOTHING_ID: return null;
            case GUI_PLAYER_INVENTORY_ID:
                if(ExtendedPlayer.get(player).isBackpackOpened)
                    ExtendedPlayer.get(player).backpackUser.openGui(MCDayZ.instance, GUI_NOTHING_ID, world, x, y, z);
                return new ContainerPlayerCustom(player, player.inventory, ExtendedPlayer.get(player).inventory);
            case GUI_BACKPACK_ID: {
                List<Entity> list = world.getEntitiesWithinAABB(Entity.class, AxisAlignedBB.getBoundingBox
                        (x - .5F, y - .5F, z - .5F, x + .5F, y + .5F, z + .5F));
                for (Entity e : list) {
                    if(e == player) continue;
                    if(e instanceof EntityPlayer) {
                        if (ExtendedPlayer.get((EntityPlayer) e).inventory.getStackInSlot(0) != null) {
                            return new ContainerBackpack(player, player.inventory, e);
                        }
                    } else return new ContainerBackpack(player, player.inventory, e);
                }
            } break;
            case GUI_CHEST_ID: return new ContainerChest(player, (TileEntityLootSpawner) world.getTileEntity(x, y, z));
        }
        return null;
    }

    @Override
    public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
        switch (ID) {
            case GUI_NOTHING_ID: return null;
            case GUI_PLAYER_INVENTORY_ID:
                return new GuiPlayerInventoryCustom(player, player.inventory, ExtendedPlayer.get(player).inventory);
            case GUI_BACKPACK_ID: {
                List<Entity> list = world.getEntitiesWithinAABB(Entity.class, AxisAlignedBB.getBoundingBox
                        (x - .5F, y - .5F, z - .5F, x + .5F, y + .5F, z + .5F));
                for (Entity e : list) {
                    if(e == player) continue;
                    if(e instanceof EntityPlayer) {
                        if (ExtendedPlayer.get((EntityPlayer) e).inventory.getStackInSlot(0) != null) {
                            return new GuiBackpack(player, player.inventory, e);
                        }
                    } else return new GuiBackpack(player, player.inventory, e);
                }
            } break;
            case GUI_CHEST_ID: return new GuiChest(player, (TileEntityLootSpawner) world.getTileEntity(x, y, z));
        }
        return null;
    }
}
