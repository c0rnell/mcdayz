package ru.xlv.mcdayz.gui;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiInventory;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Slot;
import net.minecraft.item.Item;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import ru.xlv.mcdayz.MCDayZ;
import ru.xlv.mcdayz.container.ContainerPlayerCustom;
import ru.xlv.mcdayz.gui.overlay.*;
import ru.xlv.mcdayz.item.ItemBackpack;
import ru.xlv.mcdayz.item.ItemBase;
import ru.xlv.mcdayz.network.PacketHandler;
import ru.xlv.mcdayz.utils.ExtendedPlayer;
import ru.xlv.mcdayz.utils.InventoryPlayerCustom;
import ru.xlv.mcdayz.utils.MCDayZConfig;
import ru.xlv.mcdayz.utils.Utils;

/**
 * Created by Xlv on 10.12.2017.
 */
public class GuiPlayerInventoryCustom extends GuiContainerCustom implements IOverlayHandler {

    private int xSize_lo;
    private int ySize_lo;

    private OverlayListedMenu currentOverlayElement;
    private int clickedSlotIndex = -1;
    private final InventoryPlayerCustom inventory;
    private EntityPlayer player;

    private ResourceLocation texture = new ResourceLocation(MCDayZ.MODID, "textures/gui/inventory.png");

    GuiPlayerInventoryCustom(EntityPlayer player, InventoryPlayer inventoryPlayer, InventoryPlayerCustom inventoryCustom) {
        super(new ContainerPlayerCustom(player, inventoryPlayer, inventoryCustom));
        this.inventory = inventoryCustom;
        this.player = player;
    }

    @Override
    public void initGui() {
        ySize = 165;
        super.initGui();
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float f) {
        super.drawScreen(mouseX, mouseY, f);
        xSize_lo = mouseX;
        ySize_lo = mouseY;
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float f, int mouseX, int mouseY) {
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        mc.getTextureManager().bindTexture(texture);
        drawTexturedModalRect(guiLeft, guiTop, 0, 0, xSize, ySize);
        GuiInventory.func_147046_a(guiLeft + 88, guiTop + 75, 30,
                guiLeft + 51 - xSize_lo, guiTop + 25 - ySize_lo, mc.thePlayer);
        mc.getTextureManager().bindTexture(texture);

        int ux;
        for (int i = 0; i < 3; i++) {
            for(int k = 0; k < 9; k++) {
                if (inventory.getStackInSlot(0) != null && inventory.getStackInSlot(0).getItem() instanceof ItemBackpack
                        && k + (i + 1) * 9 < ((ItemBackpack) inventory.getStackInSlot(0).getItem()).getSlotCount() + 9) {
                    ux = 7;
                } else {
                    ux = 25;
                }
                drawTexturedModalRect(guiLeft + 7 + k * 18, guiTop + 84 + (i * 18), ux, ySize, 18, 18);
            }
        }
        if(inventory.getStackInSlot(0) == null)
            DrawHelper.drawBackpackIcon(this, guiLeft + 115, guiTop + 26);
    }

    @Override
    protected void drawGuiContainerForegroundLayer(int p_146979_1_, int p_146979_2_) {
        super.drawGuiContainerForegroundLayer(p_146979_1_, p_146979_2_);
        if(currentOverlayElement != null) {
            GL11.glPushMatrix();
            GL11.glDisable(GL11.GL_LIGHTING);
            GL11.glDisable(GL11.GL_DEPTH_TEST);
            GL11.glTranslatef(-guiLeft, -guiTop, 64F);
            currentOverlayElement.draw(width, height, p_146979_1_, p_146979_2_);
            GL11.glEnable(GL11.GL_LIGHTING);
            GL11.glEnable(GL11.GL_DEPTH_TEST);
            GL11.glPopMatrix();
        }
    }

    @Override
    protected void mouseClicked(int x, int y, int id) {
        boolean flag = false;
        if(currentOverlayElement != null && id == 0) {
            if(x >= currentOverlayElement.x && x <= currentOverlayElement.x + xSize
                    && y >= currentOverlayElement.y && y <= currentOverlayElement.y + ySize) {
                    currentOverlayElement.click(x, y);
                    flag = true;
            }
        } else if(id == 1 && currentOverlayElement == null) {
            final Slot slot = getSlotAtPosition(x, y);
            if (slot != null && slot.getHasStack()) {
                if (slot.getStack().getItem() instanceof ItemBase && ((ItemBase) slot.getStack().getItem()).hasMenu()) {
                    clickedSlotIndex = slot.getSlotIndex();
                    showOverlay(x, y);
                    return;
                }
            } else if(slot == null && x >= guiLeft + 62 && x < guiLeft + 134 && y >= guiTop + 8 && y < guiTop + 79) {
                showOverlay(x, y);
                return;
            }
        }
        closeOverlay();
        if(!flag) super.mouseClicked(x, y, id);
    }

    @Override
    public void showOverlay(int x, int y) {
        currentOverlayElement = new OverlayListedMenu(x, y);
        final int clickedSlotIndex = this.clickedSlotIndex;
        final Slot slot = clickedSlotIndex == -1 ? null : (Slot) inventorySlots.inventorySlots.get(clickedSlotIndex);
        if(slot == null) {
            currentOverlayElement.addMenuElement("Check for bites", new SimpleTask() {
                @Override
                public void execute() {
                    IOverlayElement e = OverlayManager.getOverlayElement(OverlayManager.GUI_OVERLAY_PROGRESS_BAR_ID, 200);
                    ((OverlayProgressBar) e).setDestroyTask(new SimpleTask() {
                        @Override
                        public void execute() {
                            RenderGameOverlay.hasInfection = ExtendedPlayer.get(Minecraft.getMinecraft().thePlayer).hasInfection;
                            if(RenderGameOverlay.hasInfection)
                                Minecraft.getMinecraft().thePlayer.addChatMessage(new ChatComponentText(I18n.format("mcdayz.message.infectionFound")));
                            else
                                Minecraft.getMinecraft().thePlayer.addChatMessage(new ChatComponentText(I18n.format("mcdayz.message.infectionNotFound")));
                        }
                    });
                    ((OverlayProgressBar) e).setText(I18n.format("gui.overlay.progressBarTextChecking"));
                    mc.displayGuiScreen(null);
                    OverlayManager.showOverlay(e);
                }
            });
        } else {
            if (((ItemBase) slot.getStack().getItem()).canBeUsed()) {
                currentOverlayElement.addMenuElement("Use", new SimpleTask() {
                    @Override
                    public void execute() {
                        IOverlayElement e = OverlayManager.getOverlayElement(OverlayManager.GUI_OVERLAY_PROGRESS_BAR_ID, Utils.getUsingTimeFor(slot.getStack().getItem()));
                        ((OverlayProgressBar) e).setDestroyTask(new SimpleTask() {
                            @Override
                            public void execute() {
                                PacketHandler.sendPacketItemUse(clickedSlotIndex);
                            }
                        });
                        mc.displayGuiScreen(null);
                        OverlayManager.showOverlay(e);
                    }
                });
            }
            if (((ItemBase) slot.getStack().getItem()).canBeRecycled()) {
                currentOverlayElement.addMenuElement("Recycle", new SimpleTask() {
                    @Override
                    public void execute() {
                        IOverlayElement e = OverlayManager.getOverlayElement(OverlayManager.GUI_OVERLAY_PROGRESS_BAR_ID, MCDayZConfig.recyclingTime);
                        ((OverlayProgressBar) e).setText(I18n.format("gui.overlay.progressBarTextRecycling"));
                        ((OverlayProgressBar) e).setDestroyTask(new SimpleTask() {
                            @Override
                            public void execute() {
                                PacketHandler.sendPacketItemRecycle(clickedSlotIndex);
                            }
                        });
                        mc.displayGuiScreen(null);
                        OverlayManager.showOverlay(e);
                    }
                });
            }
        }
        this.clickedSlotIndex = -1;
    }

    private int getItemSlotIndex(Item item) {
        for (int i = 0; i < mc.thePlayer.inventory.mainInventory.length; i++) {
            if (mc.thePlayer.inventory.mainInventory[i] != null
                    && mc.thePlayer.inventory.mainInventory[i].getItem() == item) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public void closeOverlay() {
        currentOverlayElement = null;
    }

    private Slot getSlotAtPosition(int p_146975_1_, int p_146975_2_) {
        for (int k = 0; k < this.inventorySlots.inventorySlots.size(); ++k) {
            Slot slot = (Slot)this.inventorySlots.inventorySlots.get(k);
            if (this.isMouseOverSlot(slot, p_146975_1_, p_146975_2_)) {
                return slot;
            }
        }

        return null;
    }

    @Override
    protected boolean isMouseOverSlot(Slot slot, int x, int y) {
        if(currentOverlayElement != null) {
            if (x >= currentOverlayElement.x && x <= currentOverlayElement.x + xSize
                    && y >= currentOverlayElement.y && y <= currentOverlayElement.y + ySize) {
                return false;
            }
            if(slot.getSlotIndex() == clickedSlotIndex && x >= slot.xDisplayPosition && x <= slot.xDisplayPosition + 16
                    && y >= slot.yDisplayPosition && y <= slot.yDisplayPosition + 16) {
                return false;
            }
        }
        return super.isMouseOverSlot(slot, x, y);
    }
}