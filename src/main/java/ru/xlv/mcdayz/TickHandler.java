package ru.xlv.mcdayz;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.PlayerEvent;
import cpw.mods.fml.common.gameevent.TickEvent;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.AxisAlignedBB;
import ru.xlv.mcdayz.item.ItemBackpack;
import ru.xlv.mcdayz.network.PacketHandler;
import ru.xlv.mcdayz.utils.ExtendedPlayer;
import ru.xlv.mcdayz.utils.MCDayZConfig;
import ru.xlv.mcdayz.utils.PlayerSpawnData;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static ru.xlv.mcdayz.utils.MCDayZConfig.*;

public class TickHandler {

    private int sec;
    private int sec2;
    private int sec4;

    public TickHandler() {
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                sec++;
                sec2++;
                sec4++;
            }
        }, 0, 1000);
    }

    @SubscribeEvent
    public void playerTick(TickEvent.PlayerTickEvent event) {
        ExtendedPlayer extendedPlayer = ExtendedPlayer.get(event.player);
        if(sec4 > 3) {
            sec4 = 0;
            if(extendedPlayer.food <= 0) {
                event.player.attackEntityFrom(MCDayZ.damageSourceFood, foodDamage);
            }
            if(extendedPlayer.thirst <= 0) {
                event.player.attackEntityFrom(MCDayZ.damageSourceThirst, thirstDamage);
            }
        }
        if(sec2 > 1) {
            sec2 = 0;
            if(event.player.isEntityAlive()) {
                if(extendedPlayer.hasBleeding && !event.player.capabilities.isCreativeMode) {
                    event.player.attackEntityFrom(MCDayZ.damageSourceBleeding, bleedingDamage);
                }
                if (extendedPlayer.hasFracture && !event.player.capabilities.isCreativeMode) {
                    event.player.attackEntityFrom(MCDayZ.damageSourceFracture, fractureDamage);
                }
            }
        }
        if(sec > 0) {
            sec = 0;
            if(extendedPlayer.isFaint) {
                extendedPlayer.faintTimer++;
                if(extendedPlayer.faintTimer >= MCDayZConfig.faintTimeSec) extendedPlayer.removeUnconscious();
            }
            if(extendedPlayer.food > 0) extendedPlayer.food--;
            if(extendedPlayer.thirst > 0) extendedPlayer.thirst--;
            if(event.player.isEntityAlive()) {
                if(extendedPlayer.hasInfection && !event.player.capabilities.isCreativeMode) {
                    if(extendedPlayer.infectionTimer >= MCDayZConfig.seriousInfectionTimeSec) {
                        event.player.attackEntityFrom(MCDayZ.damageSourceInfection, infectionDamage);
//                    event.player.addPotionEffect(new PotionEffect(Potion.confusion.id, 60, 1));
                        event.player.getAttributeMap().getAttributeInstance(SharedMonsterAttributes.maxHealth)
                                .setBaseValue(event.player.getMaxHealth() - 1);
                    } else extendedPlayer.infectionTimer++;
                }
            }
            if(event.player.getFoodStats().getFoodLevel() >= 18) event.player.getFoodStats().addStats(-3, 1);
            if(event.player.getFoodStats().getFoodLevel() < 10) event.player.getFoodStats().addStats(1, 1);
            ItemStack backpack = extendedPlayer.inventory.getStackInSlot(0);
            if (backpack != null && backpack.getItem() instanceof ItemBackpack) {
                for (int i = 9; i < 36; i++) {
                    if (i > ((ItemBackpack) backpack.getItem()).getSlotCount() + 9) {
                        ItemStack itemstack = event.player.inventory.getStackInSlotOnClosing(i);

                        if (itemstack != null) {
                            event.player.entityDropItem(itemstack, 0);
                        }
                    }
                }
            } else {
                for (int i = 9; i < 36; i++) {
                    ItemStack itemstack = event.player.inventory.getStackInSlotOnClosing(i);

                    if (itemstack != null) {
                        event.player.entityDropItem(itemstack, 0);
                    }
                }
            }
            PacketHandler.sendPacketUpdateTo(event.player);
        }
        if(extendedPlayer.hasFracture) extendedPlayer.addFracture();
        if(extendedPlayer.isFaint) extendedPlayer.addFaint();
        extendedPlayer.updateUsing();
        extendedPlayer.updateBackpackUsing();
    }

    @SubscribeEvent
    public void event(PlayerEvent.PlayerLoggedOutEvent event) {
        ExtendedPlayer extendedPlayer = ExtendedPlayer.get(event.player);
        if(extendedPlayer.isUsing) {
            extendedPlayer.stopUsing();
        }
        event.player.getAttributeMap().getAttributeInstance(SharedMonsterAttributes.movementSpeed).setBaseValue(0.10000000149011612D);
    }

    @SubscribeEvent
    public void event(PlayerEvent.PlayerRespawnEvent event) {
        ExtendedPlayer extendedPlayer = ExtendedPlayer.get(event.player);
        if(extendedPlayer == null) ExtendedPlayer.register(event.player);
        else PacketHandler.sendPacketUpdateTo(event.player);
        if(MCDayZConfig.enableRandomPlayerSpawning && !PlayerSpawnData.playerSpawnDataList.isEmpty()) {
            int i = 0;
            PlayerSpawnData psd = PlayerSpawnData.playerSpawnDataList.get(i);
            int r = MCDayZConfig.playerSpawnRadiusOfCheckingForPlayers;
            boolean flag = false;
            while(!flag) {
                if(psd.checkForPlayersAround) {
                    List<EntityPlayer> list = event.player.worldObj.getEntitiesWithinAABB(EntityPlayer.class,
                            AxisAlignedBB.getBoundingBox(psd.x - r, psd.y - r, psd.z - r,
                                    psd.x + r, psd.y + r, psd.z + r));
                    for(EntityPlayer e : list) {
                        if(e.isEntityAlive()) {
                            psd = PlayerSpawnData.playerSpawnDataList.get(++i);
                            break;
                        }
                    }
                } else flag = true;
            }
            event.player.setPositionAndUpdate(psd.x, psd.y, psd.z);
        }
    }
}
