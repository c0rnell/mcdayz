package ru.xlv.mcdayz.container;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import ru.xlv.mcdayz.item.ItemBackpack;
import ru.xlv.mcdayz.utils.ExtendedPlayer;

public class ContainerBase extends Container {

    void addPlayerInventory(final EntityPlayer player, int x, int y) {
        for (int i = 0; i < 9; ++i) {
            addSlotToContainer(new Slot(player.inventory, i, x + i * 18, y + 58));
        }

        for (int i = 0; i < 3; ++i) {
            for (int j = 0; j < 9; ++j) {
                this.addSlotToContainer(new Slot(player.inventory, j + (i + 1) * 9, x + j * 18, y + i * 18) {
                    @Override
                    public boolean isItemValid(ItemStack p_75214_1_) {
                        ItemStack backpack = ExtendedPlayer.get(player).inventory.getStackInSlot(0);
                        if(backpack == null) return false;
                        if(backpack.getItem() instanceof ItemBackpack) {
                            if(getSlotIndex() >= ((ItemBackpack) backpack.getItem()).getSlotCount() + 9) {
                                return false;
                            }
                        }
                        return super.isItemValid(p_75214_1_);
                    }
                });
            }
        }
    }

    void addBackpackSlot(EntityPlayer player, int x, int y) {
        addSlotToContainer(new Slot(ExtendedPlayer.get(player).inventory, 0, x, y) {
            @Override
            public boolean isItemValid(ItemStack p_75214_1_) {
                return p_75214_1_.getItem() instanceof ItemBackpack;
            }
        });
    }

    void addGlassesSlot(EntityPlayer player, int x, int y) {
        addSlotToContainer(new Slot(ExtendedPlayer.get(player).inventory, 1, x, y) {
            @Override
            public boolean isItemValid(ItemStack p_75214_1_) {
                return false;
            }
        });
    }

    @SideOnly(Side.SERVER)
    @Override
    public void onContainerClosed(EntityPlayer player) {
        super.onContainerClosed(player);

        ItemStack backpack = ExtendedPlayer.get(player).inventory.getStackInSlot(0);
        if (backpack != null && backpack.getItem() instanceof ItemBackpack) {
            for (int i = 9; i < 36; i++) {
                if (i > ((ItemBackpack) backpack.getItem()).getSlotCount() + 9) {
                    ItemStack itemstack = player.inventory.getStackInSlotOnClosing(i);

                    if (itemstack != null) {
                        player.entityDropItem(itemstack, 0);
                    }
                }
            }
        } else {
            for (int i = 9; i < 36; i++) {
                ItemStack itemstack = player.inventory.getStackInSlotOnClosing(i);

                if (itemstack != null) {
                    player.entityDropItem(itemstack, 0);
                }
            }
        }
    }

    @Override
    public boolean canInteractWith(EntityPlayer p_75145_1_) {
        return true;
    }
}
