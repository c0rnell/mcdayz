package ru.xlv.mcdayz.container;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import ru.xlv.mcdayz.entity.EntityCorpse;
import ru.xlv.mcdayz.entity.EntityTent;
import ru.xlv.mcdayz.item.Items;
import ru.xlv.mcdayz.utils.ExtendedPlayer;

public class ContainerBackpack extends ContainerBase {

    private InventoryPlayer field_111243_a;
    private Entity entity;
    private EntityPlayer player;
    private boolean hasBackpack;

    public ContainerBackpack(EntityPlayer player, IInventory par2IInventory, Entity par3EntityHorse) {
        this.field_111243_a = player.inventory;
        this.player = player;
        this.entity = par3EntityHorse;
        byte b0 = 3;
        par2IInventory.openInventory();
        int i = (b0 - 4) * 18;
        int x = 128,
                y = 128;

        IInventory inv = null;
        if(par3EntityHorse instanceof EntityTent) inv = ((EntityTent) par3EntityHorse).chest;
        if(par3EntityHorse instanceof EntityPlayer) inv = ((EntityPlayer) par3EntityHorse).inventory;
        if(par3EntityHorse instanceof EntityCorpse) inv = ((EntityCorpse) par3EntityHorse).chest;

        for (int j = 0; j < 3; ++j)
        {
            for (int k = 0; k < 9; ++k)
            {
                this.addSlotToContainer(new Slot(inv, 9 + k + j * 9, -92 + k * 18 + x, 89 + j * 18 + i + y));
            }
        }
        for (int j = 0; j < 9; ++j)
        {
            this.addSlotToContainer(new Slot(inv, j, -92 + j * 18 + x, 147 + i + y));
        }

        for (i = 0; i < 4; ++i) {
            addSlotToContainer(new SlotArmor(player, player.inventory, player.inventory.getSizeInventory() - 1 - i, 346 + x, 71 + i * 18 + y, i));
        }
        addBackpackSlot(player, 364 + x, 89 + y);
        addGlassesSlot(player, 364 + x, 71 + y);
        addPlayerInventory(player, 184 + x, 67 + y + i);
    }

    @Override
    public void detectAndSendChanges() {
        super.detectAndSendChanges();
        if(this.hasBackpack){
            InventoryPlayer inv = field_111243_a;
            if(!(inv.hasItemStack(new ItemStack(Items.backpack0)) || inv.hasItemStack(new ItemStack(Items.backpack1))
                    || inv.hasItemStack(new ItemStack(Items.backpack2)))) {
                this.player.closeScreen();
                this.hasBackpack = false;
            }
        }
    }

    public boolean canInteractWith(EntityPlayer par1EntityPlayer)
    {
        return this.field_111243_a.isUseableByPlayer(par1EntityPlayer) && this.entity.isEntityAlive() && this.entity.getDistanceToEntity(par1EntityPlayer) < 8.0F;
    }

    public ItemStack transferStackInSlot(EntityPlayer par1EntityPlayer, int par2)
    {
        ItemStack itemstack = null;
        Slot slot = (Slot)this.inventorySlots.get(par2);

        if (slot != null && slot.getHasStack())
        {
            ItemStack itemstack1 = slot.getStack();
            itemstack = itemstack1.copy();

            if (par2 < this.field_111243_a.getSizeInventory())
            {
                if (!this.mergeItemStack(itemstack1, this.field_111243_a.getSizeInventory(), this.inventorySlots.size(), true))
                {
                    return null;
                }
            }
            else if (this.getSlot(1).isItemValid(itemstack1) && !this.getSlot(1).getHasStack())
            {
                if (!this.mergeItemStack(itemstack1, 1, 2, false))
                {
                    return null;
                }
            }
            else if (this.getSlot(0).isItemValid(itemstack1))
            {
                if (!this.mergeItemStack(itemstack1, 0, 1, false))
                {
                    return null;
                }
            }
            else if (this.field_111243_a.getSizeInventory() <= 2 || !this.mergeItemStack(itemstack1, 2, this.field_111243_a.getSizeInventory(), false))
            {
                return null;
            }

            if (itemstack1.stackSize == 0)
            {
                slot.putStack((ItemStack)null);
            }
            else
            {
                slot.onSlotChanged();
            }
        }

        return itemstack;
    }

    public void onContainerClosed(EntityPlayer par1EntityPlayer)
    {
        super.onContainerClosed(par1EntityPlayer);
        this.field_111243_a.closeInventory();
    }
}
