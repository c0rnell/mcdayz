package ru.xlv.mcdayz.container;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import ru.xlv.mcdayz.tileentity.TileEntityLootSpawner;

public class ContainerChest extends ContainerBase {

    private TileEntityLootSpawner tile;

    public ContainerChest(final EntityPlayer player, TileEntityLootSpawner tile) {
        this.tile = tile;

        int x;
        int x1;
        for(x = 0; x < 3; ++x) {
            for(x1 = 0; x1 < 9; ++x1) {
                this.addSlotToContainer(new Slot(tile, x1 + x * 9, 8 + x1 * 18, 17 + x * 18));
            }
        }

        addPlayerInventory(player, 8, 84);
    }

    public ItemStack transferStackInSlot(EntityPlayer playerIn, int fromSlot) {
        ItemStack previous = null;
        Slot slot = (Slot)this.inventorySlots.get(fromSlot);
        if(slot != null && slot.getHasStack()) {
            ItemStack current = slot.getStack();
            previous = current.copy();
            if(fromSlot < 27) {
                if(!this.mergeItemStack(current, 27, 63, true)) {
                    return null;
                }
            } else if(!this.mergeItemStack(current, 0, 27, false)) {
                return null;
            }

            if(current.stackSize == 0) {
                slot.putStack((ItemStack)null);
            } else {
                slot.onSlotChanged();
            }

            if(current.stackSize == previous.stackSize) {
                return null;
            }

            slot.onPickupFromSlot(playerIn, current);
        }

        return previous;
    }

    public boolean canInteractWith(EntityPlayer player) {
        return this.tile.isUseableByPlayer(player);
    }
}
