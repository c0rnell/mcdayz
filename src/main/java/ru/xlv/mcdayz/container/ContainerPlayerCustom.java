package ru.xlv.mcdayz.container;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import ru.xlv.mcdayz.item.ItemBackpack;
import ru.xlv.mcdayz.utils.ExtendedPlayer;
import ru.xlv.mcdayz.utils.InventoryPlayerCustom;

public class ContainerPlayerCustom extends ContainerBase {

    public ContainerPlayerCustom(final EntityPlayer player, InventoryPlayer inventoryPlayer, InventoryPlayerCustom inventoryCustom) {
        int i;
        int j;

        addPlayerInventory(player, 8, 84);
        addBackpackSlot(player, 116, 26);
        addGlassesSlot(player, 116, 8);

        for (i = 0; i < 4; ++i) {
            addSlotToContainer(new SlotArmor(player, inventoryPlayer, inventoryPlayer.getSizeInventory() - 1 - i, 44, 8 + i * 18, i));
        }
    }

    @Override
    public boolean canInteractWith(EntityPlayer player) {
        return true;
    }

    public ItemStack transferStackInSlot(EntityPlayer player, int par2) {
        return null;
    }
}