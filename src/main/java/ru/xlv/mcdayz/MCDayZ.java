package ru.xlv.mcdayz;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import cpw.mods.fml.common.event.FMLServerStoppingEvent;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.registry.EntityRegistry;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.util.DamageSource;
import net.minecraftforge.common.MinecraftForge;
import ru.xlv.mcdayz.block.Blocks;
import ru.xlv.mcdayz.command.CommandHandler;
import ru.xlv.mcdayz.entity.EntityCorpse;
import ru.xlv.mcdayz.entity.EntityTent;
import ru.xlv.mcdayz.gui.GuiHandler;
import ru.xlv.mcdayz.item.Items;
import ru.xlv.mcdayz.network.PacketHandler;
import ru.xlv.mcdayz.proxy.CommonProxy;
import ru.xlv.mcdayz.tileentity.TileEntityLootSpawner;
import ru.xlv.mcdayz.tileentity.TileEntityMine;
import ru.xlv.mcdayz.utils.MCDayZConfig;
import ru.xlv.mcdayz.utils.MCDayZDamageSource;

import java.util.HashMap;

@Mod(
        modid = MCDayZ.MODID,
        name = MCDayZ.NAME,
        version = MCDayZ.VERSION
)
public class MCDayZ {

    public static final String MODID = "mcdayz";
    static final String NAME = "MCDayZ";
    static final String VERSION = "0.4.2.1";
    @Mod.Instance
    public static MCDayZ instance;

    @SidedProxy(clientSide = "ru.xlv.mcdayz.proxy.ClientProxy", serverSide = "ru.xlv.mcdayz.proxy.ServerProxy")
    private static CommonProxy proxy;

    public static DamageSource damageSourceThirst = new MCDayZDamageSource("damageSourceThirst");
    public static DamageSource damageSourceFood = new MCDayZDamageSource("damageSourceFood");
    public static DamageSource damageSourceBleeding = new MCDayZDamageSource("damageSourceBleeding");
    public static DamageSource damageSourceFracture = new MCDayZDamageSource("damageSourceFracture");
    public static DamageSource damageSourceInfection = new MCDayZDamageSource("damageSourceInfection");

    public static CreativeTabs tab = new CreativeTabs("MCDayZ") {
        @Override
        public Item getTabIconItem() {
            return Items.bigAidKit;
        }
    };
    public static CreativeTabs tab_other = new CreativeTabs("MCDayZ_other") {
        @Override
        public Item getTabIconItem() {
            return Item.getItemFromBlock(Blocks.itemSpawner);
        }
    };

    public static HashMap<String, Integer> playerBackpacks = new HashMap<String, Integer>();

    @EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        EntityRegistry.registerModEntity(EntityTent.class, "EntityTent", 70, instance, 32, 5, true);
        EntityRegistry.registerModEntity(EntityCorpse.class, "EntityCorpse", 71, instance, 32, 5, true);
        GameRegistry.registerTileEntity(TileEntityLootSpawner.class, "TileEntityLootSpawner");
        GameRegistry.registerTileEntity(TileEntityMine.class, "TileEntityMine");
        proxy.preInit(event);
        NetworkRegistry.INSTANCE.registerGuiHandler(this, new GuiHandler());
        MinecraftForge.EVENT_BUS.register(new Events());
        Blocks.register();
        Items.register();
        new PacketHandler();
    }

    @EventHandler
    public void event(FMLServerStoppingEvent event) {
        MCDayZConfig.saveServerConfig();
    }

    @EventHandler
    public void event(FMLServerStartingEvent event) {
        CommandHandler.register(event);
    }
}
